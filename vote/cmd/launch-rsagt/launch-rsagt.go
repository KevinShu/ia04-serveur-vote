package main

import (
	"fmt"

	ras "gitlab.com/KevinShu/ia04-serveur-vote/vote/restserveragent"
)

func main() {
	server := ras.NewRestServerAgent(":8080")
	server.Start()
	fmt.Scanln()
}
