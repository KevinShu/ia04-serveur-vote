# Première rendue IAO4 : ia04-serveur-vote

|  Information |   Valeur              |
| :------------ | :------------- |
| **Auteurs** | Bingqian Shu (bingqian.shu@etu.utc.fr) |
| | Chenxin Li (chenxin.li@etu.utc.fr) |
| | Sylvain Lagrue (sylvain.lagrue@utc.fr) |
| | Khaled Belahcene (khaled.belahcene@utc.fr) |

## Comment tester ?

Installer le paquet:
```
go install gitlab.com/KevinShu/ia04-serveur-vote@latest
```
Execution si la variable environnement GOPATH existe:
```
$GOPATH/bin/ia04-serveur-vote
```
Si GOPATH n'existe pas:
```
$HOME/go/bin/ia04-serveur-vote
```

Par default, le serveur va être lancé et de nombreux clients vont envoyer les requêts automatiquement. (Requêt de création de ballots, requêt de vote, requêt de résultat, requêt de nombre de requêts reçus côté serveur) 

Nous pouvons quand même envoyer des requêts manuellement en utilisant [l'extension RESTED](https://chrome.google.com/webstore/detail/rested/eelcnbccaccipfolokglfhhmapdchbfg?hl=fr&)

## Organisation des fichiers et des répertoires

5 packages:
- [ ] `main` execution du programme
- [ ] `types` stockage de toutes les structures, situé dans `/types`
- [ ] `comsoc` définition de toutes les fonctions SWF et SCF utiles, situé dans `/comsoc`
- [ ] `restclientagent` définition de client, situé dans `/vote/restclientagent`
- [ ] `restserveragent` définition de serveur, situé dans `/vote/restserveragent`

## Organisme de fonction

Le programme réalise des simulations de vote en suivant les étapes suivantes:
1. Lancement du serveur (Ecouter en :8080).
2. Un client effectue cinq requêtes `/new_ballot` consécutives pour créer différents votes (borda, majority, approval, stv, kemeny) et définit leurs id comme : `vote_borda`, `vote_majority`, `vote_approval`, `vote_stv`, `vote_kemeny`.
3. Lancer les clients votants (100 clients dans nos cas), et gérerer pour eux les préférences (un seuil pour le vote approval).
4. Les clients envoient des requêts `/vote` pour voter simultanément sur ces 5 vote.
5. Les clients dorment pendant quelques secondes (jusqu'à ce que tous les deadlines de vote sont dépassés).
6. Le serveur calcule les résultat des vote (un mécanisme mis en pratique par la fonction `calculateResult` pour chercher périodiquement s'il existe de ballot dont le deadline est dépassé, si oui, une calculation de vote sera déclenchée).
7. Les clients demandent pour chaque vote le résultat.
8. Les clients demendent le nombre de requêt total reçu par le serveur.

## Résultat
```
2022/11/07 18:26:11 démarrage du serveur...
2022/11/07 18:26:11 démarrage le client qui génére les ballots...
2022/11/07 18:26:11 création de différent ballots ...
2022/11/07 18:26:11 Listening on :8080
2022/11/07 18:26:11 [POST][ballot_creation_agt] request to create ballot, id = borda, 
2022/11/07 18:26:11 [POST][ballot_creation_agt] request to create ballot, id = majority, 
2022/11/07 18:26:11 [POST][ballot_creation_agt] request to create ballot, id = approval, 
2022/11/07 18:26:11 [POST][ballot_creation_agt] request to create ballot, id = stv, 
2022/11/07 18:26:11 [POST][ballot_creation_agt] request to create ballot, id = kemeny, 
2022/11/07 18:26:16 démarrage des clients...
2022/11/07 18:26:16 démarrage de id00
2022/11/07 18:26:16 démarrage de id12
2022/11/07 18:26:16 démarrage de id06
2022/11/07 18:26:16 démarrage de id03
2022/11/07 18:26:16 démarrage de id09
2022/11/07 18:26:16 démarrage de id10
2022/11/07 18:26:16 démarrage de id01
2022/11/07 18:26:16 démarrage de id02
2022/11/07 18:26:16 démarrage de id05
2022/11/07 18:26:16 démarrage de id55
2022/11/07 18:26:16 démarrage de id13
2022/11/07 18:26:16 démarrage de id14
2022/11/07 18:26:16 démarrage de id15
2022/11/07 18:26:16 démarrage de id16
2022/11/07 18:26:16 démarrage de id04
2022/11/07 18:26:16 démarrage de id29
2022/11/07 18:26:16 démarrage de id18
2022/11/07 18:26:16 démarrage de id17
2022/11/07 18:26:16 démarrage de id19
2022/11/07 18:26:16 démarrage de id20
2022/11/07 18:26:16 démarrage de id21
2022/11/07 18:26:16 démarrage de id22
2022/11/07 18:26:16 démarrage de id23
2022/11/07 18:26:16 démarrage de id25
2022/11/07 18:26:16 démarrage de id26
2022/11/07 18:26:16 démarrage de id27
2022/11/07 18:26:16 démarrage de id28
2022/11/07 18:26:16 démarrage de id24
2022/11/07 18:26:16 démarrage de id30
2022/11/07 18:26:16 démarrage de id31
2022/11/07 18:26:16 démarrage de id32
2022/11/07 18:26:16 démarrage de id33
2022/11/07 18:26:16 démarrage de id34
2022/11/07 18:26:16 démarrage de id35
2022/11/07 18:26:16 démarrage de id77
2022/11/07 18:26:16 démarrage de id56
2022/11/07 18:26:16 démarrage de id57
2022/11/07 18:26:16 démarrage de id58
2022/11/07 18:26:16 démarrage de id59
2022/11/07 18:26:16 démarrage de id60
2022/11/07 18:26:16 démarrage de id61
2022/11/07 18:26:16 démarrage de id62
2022/11/07 18:26:16 démarrage de id63
2022/11/07 18:26:16 démarrage de id64
2022/11/07 18:26:16 démarrage de id65
2022/11/07 18:26:16 démarrage de id66
2022/11/07 18:26:16 démarrage de id07
2022/11/07 18:26:16 démarrage de id67
2022/11/07 18:26:16 démarrage de id68
2022/11/07 18:26:16 démarrage de id08
2022/11/07 18:26:16 démarrage de id69
2022/11/07 18:26:16 démarrage de id70
2022/11/07 18:26:16 démarrage de id71
2022/11/07 18:26:16 démarrage de id72
2022/11/07 18:26:16 démarrage de id73
2022/11/07 18:26:16 démarrage de id74
2022/11/07 18:26:16 démarrage de id75
2022/11/07 18:26:16 démarrage de id76
2022/11/07 18:26:16 démarrage de id45
2022/11/07 18:26:16 démarrage de id36
2022/11/07 18:26:16 démarrage de id37
2022/11/07 18:26:16 démarrage de id38
2022/11/07 18:26:16 démarrage de id39
2022/11/07 18:26:16 démarrage de id40
2022/11/07 18:26:16 démarrage de id41
2022/11/07 18:26:16 démarrage de id42
2022/11/07 18:26:16 démarrage de id43
2022/11/07 18:26:16 démarrage de id44
2022/11/07 18:26:16 démarrage de id11
2022/11/07 18:26:16 démarrage de id88
2022/11/07 18:26:16 démarrage de id78
2022/11/07 18:26:16 démarrage de id79
2022/11/07 18:26:16 démarrage de id80
2022/11/07 18:26:16 démarrage de id93
2022/11/07 18:26:16 démarrage de id81
2022/11/07 18:26:16 démarrage de id82
2022/11/07 18:26:16 démarrage de id89
2022/11/07 18:26:16 démarrage de id83
2022/11/07 18:26:16 démarrage de id90
2022/11/07 18:26:16 démarrage de id84
2022/11/07 18:26:16 démarrage de id85
2022/11/07 18:26:16 démarrage de id86
2022/11/07 18:26:16 démarrage de id91
2022/11/07 18:26:16 démarrage de id87
2022/11/07 18:26:16 démarrage de id92
2022/11/07 18:26:16 démarrage de id46
2022/11/07 18:26:16 démarrage de id95
2022/11/07 18:26:16 démarrage de id97
2022/11/07 18:26:16 démarrage de id51
2022/11/07 18:26:16 démarrage de id52
2022/11/07 18:26:16 démarrage de id53
2022/11/07 18:26:16 démarrage de id54
2022/11/07 18:26:16 démarrage de id50
2022/11/07 18:26:16 démarrage de id47
2022/11/07 18:26:16 démarrage de id98
2022/11/07 18:26:16 démarrage de id48
2022/11/07 18:26:16 démarrage de id49
2022/11/07 18:26:16 démarrage de id99
2022/11/07 18:26:16 démarrage de id96
2022/11/07 18:26:16 démarrage de id94
2022/11/07 18:26:16 [POST][id12] voting to vote_borda, preferences = [3 4 6 7 1 2 9 10 5 8], vote succesful
2022/11/07 18:26:16 [POST][id10] voting to vote_borda, preferences = [1 8 4 9 10 3 7 6 2 5], vote succesful
2022/11/07 18:26:16 [POST][id00] voting to vote_borda, preferences = [10 5 3 7 9 1 4 2 8 6], vote succesful
2022/11/07 18:26:16 [POST][id03] voting to vote_borda, preferences = [1 5 3 6 10 8 4 2 7 9], vote succesful
2022/11/07 18:26:16 [POST][id01] voting to vote_borda, preferences = [4 1 3 6 5 10 7 2 8 9], vote succesful
2022/11/07 18:26:16 [POST][id53] voting to vote_borda, preferences = [10 2 8 5 9 4 3 6 7 1], vote succesful
2022/11/07 18:26:16 [POST][id56] voting to vote_borda, preferences = [9 8 3 4 6 1 5 7 2 10], vote succesful
2022/11/07 18:26:16 [POST][id99] voting to vote_borda, preferences = [7 9 1 8 3 6 2 10 4 5], vote succesful
2022/11/07 18:26:16 [POST][id20] voting to vote_borda, preferences = [1 2 10 6 8 4 3 9 7 5], vote succesful
2022/11/07 18:26:16 [POST][id94] voting to vote_borda, preferences = [9 3 5 4 6 7 2 10 1 8], vote succesful
2022/11/07 18:26:16 [POST][id33] voting to vote_borda, preferences = [3 4 2 10 6 1 7 8 5 9], vote succesful
2022/11/07 18:26:16 [POST][id13] voting to vote_borda, preferences = [8 5 3 7 10 6 2 9 4 1], vote succesful
2022/11/07 18:26:16 [POST][id29] voting to vote_borda, preferences = [3 6 7 4 8 5 2 10 1 9], vote succesful
2022/11/07 18:26:16 [POST][id34] voting to vote_borda, preferences = [10 1 3 7 2 5 8 6 9 4], vote succesful
2022/11/07 18:26:16 [POST][id26] voting to vote_borda, preferences = [7 3 1 10 5 2 9 8 4 6], vote succesful
2022/11/07 18:26:16 [POST][id57] voting to vote_borda, preferences = [7 3 2 1 5 4 6 9 10 8], vote succesful
2022/11/07 18:26:16 [POST][id60] voting to vote_borda, preferences = [10 1 7 4 6 3 5 8 2 9], vote succesful
2022/11/07 18:26:16 [POST][id17] voting to vote_borda, preferences = [8 9 7 3 2 10 5 1 4 6], vote succesful
2022/11/07 18:26:16 [POST][id63] voting to vote_borda, preferences = [5 3 2 7 9 6 1 8 10 4], vote succesful
2022/11/07 18:26:16 [POST][id58] voting to vote_borda, preferences = [6 1 7 2 5 3 10 9 4 8], vote succesful
2022/11/07 18:26:16 [POST][id28] voting to vote_borda, preferences = [6 7 8 4 10 1 9 3 2 5], vote succesful
2022/11/07 18:26:16 [POST][id18] voting to vote_borda, preferences = [8 6 10 3 1 9 4 7 5 2], vote succesful
2022/11/07 18:26:16 [POST][id49] voting to vote_borda, preferences = [8 4 9 3 10 1 6 7 5 2], vote succesful
2022/11/07 18:26:16 [POST][id55] voting to vote_borda, preferences = [3 10 1 9 7 2 8 4 6 5], vote succesful
2022/11/07 18:26:16 [POST][id43] voting to vote_borda, preferences = [5 9 4 7 6 1 2 3 8 10], vote succesful
2022/11/07 18:26:16 [POST][id76] voting to vote_borda, preferences = [7 9 5 2 3 6 4 8 1 10], vote succesful
2022/11/07 18:26:16 [POST][id14] voting to vote_borda, preferences = [3 2 6 7 10 5 1 8 4 9], vote succesful
2022/11/07 18:26:16 [POST][id40] voting to vote_borda, preferences = [3 2 6 5 10 9 8 1 4 7], vote succesful
2022/11/07 18:26:16 [POST][id16] voting to vote_borda, preferences = [5 2 1 10 3 9 7 8 4 6], vote succesful
2022/11/07 18:26:16 [POST][id79] voting to vote_borda, preferences = [9 10 6 3 4 5 2 7 1 8], vote succesful
2022/11/07 18:26:16 [POST][id25] voting to vote_borda, preferences = [6 9 4 7 2 3 8 10 1 5], vote succesful
2022/11/07 18:26:16 [POST][id77] voting to vote_borda, preferences = [7 8 9 1 6 2 4 10 3 5], vote succesful
2022/11/07 18:26:16 [POST][id59] voting to vote_borda, preferences = [2 4 7 10 6 8 9 3 5 1], vote succesful
2022/11/07 18:26:16 [POST][id93] voting to vote_borda, preferences = [6 10 2 3 5 1 9 8 4 7], vote succesful
2022/11/07 18:26:16 [POST][id80] voting to vote_borda, preferences = [7 10 4 2 3 6 5 9 1 8], vote succesful
2022/11/07 18:26:16 [POST][id30] voting to vote_borda, preferences = [3 9 4 1 6 5 8 7 10 2], vote succesful
2022/11/07 18:26:16 [POST][id75] voting to vote_borda, preferences = [4 10 8 7 1 3 9 2 6 5], vote succesful
2022/11/07 18:26:16 [POST][id32] voting to vote_borda, preferences = [9 1 2 8 6 10 3 4 5 7], vote succesful
2022/11/07 18:26:16 [POST][id96] voting to vote_borda, preferences = [1 8 9 7 4 2 6 3 10 5], vote succesful
2022/11/07 18:26:16 [POST][id02] voting to vote_borda, preferences = [3 1 5 4 9 2 6 8 10 7], vote succesful
2022/11/07 18:26:16 [POST][id64] voting to vote_borda, preferences = [9 5 3 2 6 10 7 4 1 8], vote succesful
2022/11/07 18:26:16 [POST][id15] voting to vote_borda, preferences = [6 1 2 9 10 3 5 8 7 4], vote succesful
2022/11/07 18:26:16 [POST][id38] voting to vote_borda, preferences = [1 6 10 8 2 5 4 7 9 3], vote succesful
2022/11/07 18:26:16 [POST][id24] voting to vote_borda, preferences = [8 4 3 7 6 10 1 9 5 2], vote succesful
2022/11/07 18:26:16 [POST][id74] voting to vote_borda, preferences = [9 8 5 10 6 7 2 1 3 4], vote succesful
2022/11/07 18:26:16 [POST][id53] voting to vote_majority, preferences = [10 2 8 5 9 4 3 6 7 1], vote succesful
2022/11/07 18:26:16 [POST][id52] voting to vote_borda, preferences = [7 5 10 9 1 3 2 8 6 4], vote succesful
2022/11/07 18:26:16 [POST][id12] voting to vote_majority, preferences = [3 4 6 7 1 2 9 10 5 8], vote succesful
2022/11/07 18:26:16 [POST][id01] voting to vote_majority, preferences = [4 1 3 6 5 10 7 2 8 9], vote succesful
2022/11/07 18:26:16 [POST][id68] voting to vote_borda, preferences = [2 1 7 8 6 4 3 9 10 5], vote succesful
2022/11/07 18:26:16 [POST][id67] voting to vote_borda, preferences = [8 1 3 5 2 10 6 4 7 9], vote succesful
2022/11/07 18:26:16 [POST][id47] voting to vote_borda, preferences = [2 9 8 7 1 10 3 4 5 6], vote succesful
2022/11/07 18:26:16 [POST][id04] voting to vote_borda, preferences = [9 8 2 6 5 1 7 3 4 10], vote succesful
2022/11/07 18:26:16 [POST][id83] voting to vote_borda, preferences = [5 7 6 2 3 9 1 4 8 10], vote succesful
2022/11/07 18:26:16 [POST][id35] voting to vote_borda, preferences = [1 3 9 8 10 2 5 7 4 6], vote succesful
2022/11/07 18:26:16 [POST][id60] voting to vote_majority, preferences = [10 1 7 4 6 3 5 8 2 9], vote succesful
2022/11/07 18:26:16 [POST][id97] voting to vote_borda, preferences = [8 9 10 3 6 2 4 5 7 1], vote succesful
2022/11/07 18:26:16 [POST][id07] voting to vote_borda, preferences = [2 8 3 10 4 1 5 9 6 7], vote succesful
2022/11/07 18:26:16 [POST][id23] voting to vote_borda, preferences = [7 2 5 1 9 10 4 6 8 3], vote succesful
2022/11/07 18:26:16 [POST][id99] voting to vote_majority, preferences = [7 9 1 8 3 6 2 10 4 5], vote succesful
2022/11/07 18:26:16 [POST][id46] voting to vote_borda, preferences = [7 5 4 2 6 1 8 3 10 9], vote succesful
2022/11/07 18:26:16 [POST][id06] voting to vote_borda, preferences = [6 7 4 5 10 2 9 1 8 3], vote succesful
2022/11/07 18:26:16 [POST][id31] voting to vote_borda, preferences = [8 7 3 9 4 10 6 2 1 5], vote succesful
2022/11/07 18:26:16 [POST][id09] voting to vote_borda, preferences = [10 3 8 7 9 6 4 5 2 1], vote succesful
2022/11/07 18:26:16 [POST][id54] voting to vote_borda, preferences = [5 10 6 8 9 3 7 2 4 1], vote succesful
2022/11/07 18:26:16 [POST][id48] voting to vote_borda, preferences = [9 6 2 1 10 7 3 5 4 8], vote succesful
2022/11/07 18:26:16 [POST][id10] voting to vote_majority, preferences = [1 8 4 9 10 3 7 6 2 5], vote succesful
2022/11/07 18:26:16 [POST][id19] voting to vote_borda, preferences = [3 1 9 8 6 7 4 10 2 5], vote succesful
2022/11/07 18:26:16 [POST][id00] voting to vote_majority, preferences = [10 5 3 7 9 1 4 2 8 6], vote succesful
2022/11/07 18:26:16 [POST][id71] voting to vote_borda, preferences = [10 4 9 7 2 1 6 5 8 3], vote succesful
2022/11/07 18:26:16 [POST][id66] voting to vote_borda, preferences = [3 9 5 1 7 6 4 10 2 8], vote succesful
2022/11/07 18:26:16 [POST][id55] voting to vote_majority, preferences = [3 10 1 9 7 2 8 4 6 5], vote succesful
2022/11/07 18:26:16 [POST][id62] voting to vote_borda, preferences = [7 9 1 8 3 5 6 2 10 4], vote succesful
2022/11/07 18:26:16 [POST][id56] voting to vote_majority, preferences = [9 8 3 4 6 1 5 7 2 10], vote succesful
2022/11/07 18:26:16 [POST][id44] voting to vote_borda, preferences = [1 9 2 4 3 7 10 8 5 6], vote succesful
2022/11/07 18:26:16 [POST][id70] voting to vote_borda, preferences = [6 1 4 3 9 7 5 8 10 2], vote succesful
2022/11/07 18:26:16 [POST][id51] voting to vote_borda, preferences = [1 8 3 9 6 10 4 7 2 5], vote succesful
2022/11/07 18:26:16 [POST][id08] voting to vote_borda, preferences = [4 6 3 10 1 5 2 8 7 9], vote succesful
2022/11/07 18:26:16 [POST][id95] voting to vote_borda, preferences = [7 4 9 8 3 1 10 6 5 2], vote succesful
2022/11/07 18:26:16 [POST][id88] voting to vote_borda, preferences = [5 10 6 9 4 3 2 8 1 7], vote succesful
2022/11/07 18:26:16 [POST][id42] voting to vote_borda, preferences = [8 6 5 1 10 4 2 3 9 7], vote succesful
2022/11/07 18:26:16 [POST][id75] voting to vote_majority, preferences = [4 10 8 7 1 3 9 2 6 5], vote succesful
2022/11/07 18:26:16 [POST][id90] voting to vote_borda, preferences = [6 2 10 3 4 1 8 5 9 7], vote succesful
2022/11/07 18:26:16 [POST][id84] voting to vote_borda, preferences = [4 3 9 2 8 7 1 5 10 6], vote succesful
2022/11/07 18:26:16 [POST][id98] voting to vote_borda, preferences = [8 4 9 3 2 10 1 5 6 7], vote succesful
2022/11/07 18:26:16 [POST][id91] voting to vote_borda, preferences = [6 2 5 10 8 1 4 9 3 7], vote succesful
2022/11/07 18:26:16 [POST][id03] voting to vote_majority, preferences = [1 5 3 6 10 8 4 2 7 9], vote succesful
2022/11/07 18:26:16 [POST][id92] voting to vote_borda, preferences = [9 6 10 2 7 4 5 3 8 1], vote succesful
2022/11/07 18:26:16 [POST][id45] voting to vote_borda, preferences = [5 3 1 10 4 2 7 6 9 8], vote succesful
2022/11/07 18:26:16 [POST][id05] voting to vote_borda, preferences = [7 1 2 9 3 8 10 6 4 5], vote succesful
2022/11/07 18:26:16 [POST][id69] voting to vote_borda, preferences = [6 5 1 9 10 7 2 3 4 8], vote succesful
2022/11/07 18:26:16 [POST][id73] voting to vote_borda, preferences = [9 2 5 7 6 1 3 4 8 10], vote succesful
2022/11/07 18:26:16 [POST][id65] voting to vote_borda, preferences = [4 5 2 8 6 7 1 10 3 9], vote succesful
2022/11/07 18:26:16 [POST][id50] voting to vote_borda, preferences = [8 3 1 4 2 10 7 5 9 6], vote succesful
2022/11/07 18:26:16 [POST][id63] voting to vote_majority, preferences = [5 3 2 7 9 6 1 8 10 4], vote succesful
2022/11/07 18:26:16 [POST][id36] voting to vote_borda, preferences = [2 4 5 7 10 3 9 6 8 1], vote succesful
2022/11/07 18:26:16 [POST][id11] voting to vote_borda, preferences = [9 2 10 8 7 6 3 1 5 4], vote succesful
2022/11/07 18:26:16 [POST][id34] voting to vote_majority, preferences = [10 1 3 7 2 5 8 6 9 4], vote succesful
2022/11/07 18:26:16 [POST][id41] voting to vote_borda, preferences = [1 5 6 7 10 2 8 9 3 4], vote succesful
2022/11/07 18:26:16 [POST][id21] voting to vote_borda, preferences = [10 4 8 1 6 7 9 2 3 5], vote succesful
2022/11/07 18:26:16 [POST][id30] voting to vote_majority, preferences = [3 9 4 1 6 5 8 7 10 2], vote succesful
2022/11/07 18:26:16 [POST][id71] voting to vote_majority, preferences = [10 4 9 7 2 1 6 5 8 3], vote succesful
2022/11/07 18:26:16 [POST][id18] voting to vote_majority, preferences = [8 6 10 3 1 9 4 7 5 2], vote succesful
2022/11/07 18:26:16 [POST][id81] voting to vote_borda, preferences = [1 7 8 4 9 2 5 10 6 3], vote succesful
2022/11/07 18:26:16 [POST][id27] voting to vote_borda, preferences = [4 2 3 5 9 10 8 7 6 1], vote succesful
2022/11/07 18:26:16 [POST][id37] voting to vote_borda, preferences = [4 3 5 9 2 6 1 7 8 10], vote succesful
2022/11/07 18:26:16 [POST][id61] voting to vote_borda, preferences = [8 2 7 3 10 5 1 9 6 4], vote succesful
2022/11/07 18:26:16 [POST][id33] voting to vote_majority, preferences = [3 4 2 10 6 1 7 8 5 9], vote succesful
2022/11/07 18:26:16 [POST][id20] voting to vote_majority, preferences = [1 2 10 6 8 4 3 9 7 5], vote succesful
2022/11/07 18:26:16 [POST][id16] voting to vote_majority, preferences = [5 2 1 10 3 9 7 8 4 6], vote succesful
2022/11/07 18:26:16 [POST][id28] voting to vote_majority, preferences = [6 7 8 4 10 1 9 3 2 5], vote succesful
2022/11/07 18:26:16 [POST][id26] voting to vote_majority, preferences = [7 3 1 10 5 2 9 8 4 6], vote succesful
2022/11/07 18:26:16 [POST][id32] voting to vote_majority, preferences = [9 1 2 8 6 10 3 4 5 7], vote succesful
2022/11/07 18:26:16 [POST][id76] voting to vote_majority, preferences = [7 9 5 2 3 6 4 8 1 10], vote succesful
2022/11/07 18:26:16 [POST][id85] voting to vote_borda, preferences = [8 4 7 6 10 2 3 1 5 9], vote succesful
2022/11/07 18:26:16 [POST][id25] voting to vote_majority, preferences = [6 9 4 7 2 3 8 10 1 5], vote succesful
2022/11/07 18:26:16 [POST][id13] voting to vote_majority, preferences = [8 5 3 7 10 6 2 9 4 1], vote succesful
2022/11/07 18:26:16 [POST][id05] voting to vote_majority, preferences = [7 1 2 9 3 8 10 6 4 5], vote succesful
2022/11/07 18:26:16 [POST][id69] voting to vote_majority, preferences = [6 5 1 9 10 7 2 3 4 8], vote succesful
2022/11/07 18:26:16 [POST][id77] voting to vote_majority, preferences = [7 8 9 1 6 2 4 10 3 5], vote succesful
2022/11/07 18:26:16 [POST][id90] voting to vote_majority, preferences = [6 2 10 3 4 1 8 5 9 7], vote succesful
2022/11/07 18:26:16 [POST][id79] voting to vote_majority, preferences = [9 10 6 3 4 5 2 7 1 8], vote succesful
2022/11/07 18:26:16 [POST][id87] voting to vote_borda, preferences = [2 4 10 3 8 9 5 6 1 7], vote succesful
2022/11/07 18:26:16 [POST][id58] voting to vote_majority, preferences = [6 1 7 2 5 3 10 9 4 8], vote succesful
2022/11/07 18:26:16 [POST][id72] voting to vote_borda, preferences = [4 8 2 10 5 1 9 6 3 7], vote succesful
2022/11/07 18:26:16 [POST][id17] voting to vote_majority, preferences = [8 9 7 3 2 10 5 1 4 6], vote succesful
2022/11/07 18:26:16 [POST][id14] voting to vote_majority, preferences = [3 2 6 7 10 5 1 8 4 9], vote succesful
2022/11/07 18:26:16 [POST][id22] voting to vote_borda, preferences = [4 9 1 7 5 10 3 6 8 2], vote succesful
2022/11/07 18:26:16 [POST][id94] voting to vote_majority, preferences = [9 3 5 4 6 7 2 10 1 8], vote succesful
2022/11/07 18:26:16 [POST][id78] voting to vote_borda, preferences = [1 4 3 6 10 7 2 8 5 9], vote succesful
2022/11/07 18:26:16 [POST][id89] voting to vote_borda, preferences = [3 10 5 1 6 8 9 7 4 2], vote succesful
2022/11/07 18:26:16 [POST][id82] voting to vote_borda, preferences = [7 4 5 1 10 8 6 3 9 2], vote succesful
2022/11/07 18:26:16 [POST][id39] voting to vote_borda, preferences = [3 7 9 2 8 4 10 1 5 6], vote succesful
2022/11/07 18:26:16 [POST][id86] voting to vote_borda, preferences = [9 3 7 6 1 10 5 8 2 4], vote succesful
2022/11/07 18:26:16 [POST][id49] voting to vote_majority, preferences = [8 4 9 3 10 1 6 7 5 2], vote succesful
2022/11/07 18:26:16 [POST][id40] voting to vote_majority, preferences = [3 2 6 5 10 9 8 1 4 7], vote succesful
2022/11/07 18:26:16 [POST][id43] voting to vote_majority, preferences = [5 9 4 7 6 1 2 3 8 10], vote succesful
2022/11/07 18:26:16 [POST][id82] voting to vote_majority, preferences = [7 4 5 1 10 8 6 3 9 2], vote succesful
2022/11/07 18:26:16 [POST][id89] voting to vote_majority, preferences = [3 10 5 1 6 8 9 7 4 2], vote succesful
2022/11/07 18:26:16 [POST][id78] voting to vote_majority, preferences = [1 4 3 6 10 7 2 8 5 9], vote succesful
2022/11/07 18:26:16 [POST][id94] voting to vote_approval, preferences = [9 3 5 4 6 7 2 10 1 8], vote succesful
2022/11/07 18:26:16 [POST][id22] voting to vote_majority, preferences = [4 9 1 7 5 10 3 6 8 2], vote succesful
2022/11/07 18:26:16 [POST][id86] voting to vote_majority, preferences = [9 3 7 6 1 10 5 8 2 4], vote succesful
2022/11/07 18:26:16 [POST][id49] voting to vote_approval, preferences = [8 4 9 3 10 1 6 7 5 2], vote succesful
2022/11/07 18:26:16 [POST][id40] voting to vote_approval, preferences = [3 2 6 5 10 9 8 1 4 7], vote succesful
2022/11/07 18:26:16 [POST][id82] voting to vote_approval, preferences = [7 4 5 1 10 8 6 3 9 2], vote succesful
2022/11/07 18:26:16 [POST][id93] voting to vote_majority, preferences = [6 10 2 3 5 1 9 8 4 7], vote succesful
2022/11/07 18:26:16 [POST][id82] voting to vote_stv, preferences = [7 4 5 1 10 8 6 3 9 2], vote succesful
2022/11/07 18:26:16 [POST][id49] voting to vote_stv, preferences = [8 4 9 3 10 1 6 7 5 2], vote succesful
2022/11/07 18:26:16 [POST][id57] voting to vote_majority, preferences = [7 3 2 1 5 4 6 9 10 8], vote succesful
2022/11/07 18:26:16 [POST][id40] voting to vote_stv, preferences = [3 2 6 5 10 9 8 1 4 7], vote succesful
2022/11/07 18:26:16 [POST][id29] voting to vote_majority, preferences = [3 6 7 4 8 5 2 10 1 9], vote succesful
2022/11/07 18:26:16 [POST][id49] voting to vote_kemeny, preferences = [8 4 9 3 10 1 6 7 5 2], vote succesful
2022/11/07 18:26:16 [POST][id64] voting to vote_majority, preferences = [9 5 3 2 6 10 7 4 1 8], vote succesful
2022/11/07 18:26:16 [POST][id02] voting to vote_majority, preferences = [3 1 5 4 9 2 6 8 10 7], vote succesful
2022/11/07 18:26:16 [POST][id96] voting to vote_majority, preferences = [1 8 9 7 4 2 6 3 10 5], vote succesful
2022/11/07 18:26:16 [POST][id59] voting to vote_majority, preferences = [2 4 7 10 6 8 9 3 5 1], vote succesful
2022/11/07 18:26:16 [POST][id02] voting to vote_approval, preferences = [3 1 5 4 9 2 6 8 10 7], vote succesful
2022/11/07 18:26:16 [POST][id38] voting to vote_majority, preferences = [1 6 10 8 2 5 4 7 9 3], vote succesful
2022/11/07 18:26:16 [POST][id12] voting to vote_approval, preferences = [3 4 6 7 1 2 9 10 5 8], vote succesful
2022/11/07 18:26:16 [POST][id53] voting to vote_approval, preferences = [10 2 8 5 9 4 3 6 7 1], vote succesful
2022/11/07 18:26:16 [POST][id24] voting to vote_majority, preferences = [8 4 3 7 6 10 1 9 5 2], vote succesful
2022/11/07 18:26:16 [POST][id80] voting to vote_majority, preferences = [7 10 4 2 3 6 5 9 1 8], vote succesful
2022/11/07 18:26:16 [POST][id61] voting to vote_majority, preferences = [8 2 7 3 10 5 1 9 6 4], vote succesful
2022/11/07 18:26:16 [POST][id48] voting to vote_majority, preferences = [9 6 2 1 10 7 3 5 4 8], vote succesful
2022/11/07 18:26:16 [POST][id00] voting to vote_approval, preferences = [10 5 3 7 9 1 4 2 8 6], vote succesful
2022/11/07 18:26:16 [POST][id52] voting to vote_majority, preferences = [7 5 10 9 1 3 2 8 6 4], vote succesful
2022/11/07 18:26:16 [POST][id84] voting to vote_majority, preferences = [4 3 9 2 8 7 1 5 10 6], vote succesful
2022/11/07 18:26:16 [POST][id74] voting to vote_majority, preferences = [9 8 5 10 6 7 2 1 3 4], vote succesful
2022/11/07 18:26:16 [POST][id03] voting to vote_approval, preferences = [1 5 3 6 10 8 4 2 7 9], vote succesful
2022/11/07 18:26:16 [POST][id45] voting to vote_majority, preferences = [5 3 1 10 4 2 7 6 9 8], vote succesful
2022/11/07 18:26:16 [POST][id73] voting to vote_majority, preferences = [9 2 5 7 6 1 3 4 8 10], vote succesful
2022/11/07 18:26:16 [POST][id65] voting to vote_majority, preferences = [4 5 2 8 6 7 1 10 3 9], vote succesful
2022/11/07 18:26:16 [POST][id50] voting to vote_majority, preferences = [8 3 1 4 2 10 7 5 9 6], vote succesful
2022/11/07 18:26:16 [POST][id19] voting to vote_majority, preferences = [3 1 9 8 6 7 4 10 2 5], vote succesful
2022/11/07 18:26:16 [POST][id70] voting to vote_majority, preferences = [6 1 4 3 9 7 5 8 10 2], vote succesful
2022/11/07 18:26:16 [POST][id31] voting to vote_majority, preferences = [8 7 3 9 4 10 6 2 1 5], vote succesful
2022/11/07 18:26:16 [POST][id09] voting to vote_majority, preferences = [10 3 8 7 9 6 4 5 2 1], vote succesful
2022/11/07 18:26:16 [POST][id97] voting to vote_majority, preferences = [8 9 10 3 6 2 4 5 7 1], vote succesful
2022/11/07 18:26:16 [POST][id67] voting to vote_majority, preferences = [8 1 3 5 2 10 6 4 7 9], vote succesful
2022/11/07 18:26:16 [POST][id83] voting to vote_majority, preferences = [5 7 6 2 3 9 1 4 8 10], vote succesful
2022/11/07 18:26:16 [POST][id42] voting to vote_majority, preferences = [8 6 5 1 10 4 2 3 9 7], vote succesful
2022/11/07 18:26:16 [POST][id35] voting to vote_majority, preferences = [1 3 9 8 10 2 5 7 4 6], vote succesful
2022/11/07 18:26:16 [POST][id46] voting to vote_majority, preferences = [7 5 4 2 6 1 8 3 10 9], vote succesful
2022/11/07 18:26:16 [POST][id99] voting to vote_approval, preferences = [7 9 1 8 3 6 2 10 4 5], vote succesful
2022/11/07 18:26:16 [POST][id23] voting to vote_majority, preferences = [7 2 5 1 9 10 4 6 8 3], vote succesful
2022/11/07 18:26:16 [POST][id88] voting to vote_majority, preferences = [5 10 6 9 4 3 2 8 1 7], vote succesful
2022/11/07 18:26:16 [POST][id08] voting to vote_majority, preferences = [4 6 3 10 1 5 2 8 7 9], vote succesful
2022/11/07 18:26:16 [POST][id68] voting to vote_majority, preferences = [2 1 7 8 6 4 3 9 10 5], vote succesful
2022/11/07 18:26:16 [POST][id92] voting to vote_majority, preferences = [9 6 10 2 7 4 5 3 8 1], vote succesful
2022/11/07 18:26:16 [POST][id18] voting to vote_approval, preferences = [8 6 10 3 1 9 4 7 5 2], vote succesful
2022/11/07 18:26:16 [POST][id30] voting to vote_approval, preferences = [3 9 4 1 6 5 8 7 10 2], vote succesful
2022/11/07 18:26:16 [POST][id21] voting to vote_majority, preferences = [10 4 8 1 6 7 9 2 3 5], vote succesful
2022/11/07 18:26:16 [POST][id71] voting to vote_approval, preferences = [10 4 9 7 2 1 6 5 8 3], vote succesful
2022/11/07 18:26:16 [POST][id20] voting to vote_approval, preferences = [1 2 10 6 8 4 3 9 7 5], vote succesful
2022/11/07 18:26:16 [POST][id07] voting to vote_majority, preferences = [2 8 3 10 4 1 5 9 6 7], vote succesful
2022/11/07 18:26:16 [POST][id04] voting to vote_majority, preferences = [9 8 2 6 5 1 7 3 4 10], vote succesful
2022/11/07 18:26:16 [POST][id01] voting to vote_approval, preferences = [4 1 3 6 5 10 7 2 8 9], vote succesful
2022/11/07 18:26:16 [POST][id54] voting to vote_majority, preferences = [5 10 6 8 9 3 7 2 4 1], vote succesful
2022/11/07 18:26:16 [POST][id34] voting to vote_approval, preferences = [10 1 3 7 2 5 8 6 9 4], vote succesful
2022/11/07 18:26:16 [POST][id41] voting to vote_majority, preferences = [1 5 6 7 10 2 8 9 3 4], vote succesful
2022/11/07 18:26:16 [POST][id47] voting to vote_majority, preferences = [2 9 8 7 1 10 3 4 5 6], vote succesful
2022/11/07 18:26:16 [POST][id10] voting to vote_approval, preferences = [1 8 4 9 10 3 7 6 2 5], vote succesful
2022/11/07 18:26:16 [POST][id33] voting to vote_approval, preferences = [3 4 2 10 6 1 7 8 5 9], vote succesful
2022/11/07 18:26:16 [POST][id16] voting to vote_approval, preferences = [5 2 1 10 3 9 7 8 4 6], vote succesful
2022/11/07 18:26:16 [POST][id27] voting to vote_majority, preferences = [4 2 3 5 9 10 8 7 6 1], vote succesful
2022/11/07 18:26:16 [POST][id26] voting to vote_approval, preferences = [7 3 1 10 5 2 9 8 4 6], vote succesful
2022/11/07 18:26:16 [POST][id81] voting to vote_majority, preferences = [1 7 8 4 9 2 5 10 6 3], vote succesful
2022/11/07 18:26:16 [POST][id37] voting to vote_majority, preferences = [4 3 5 9 2 6 1 7 8 10], vote succesful
2022/11/07 18:26:16 [POST][id28] voting to vote_approval, preferences = [6 7 8 4 10 1 9 3 2 5], vote succesful
2022/11/07 18:26:16 [POST][id76] voting to vote_approval, preferences = [7 9 5 2 3 6 4 8 1 10], vote succesful
2022/11/07 18:26:16 [POST][id85] voting to vote_majority, preferences = [8 4 7 6 10 2 3 1 5 9], vote succesful
2022/11/07 18:26:16 [POST][id25] voting to vote_approval, preferences = [6 9 4 7 2 3 8 10 1 5], vote succesful
2022/11/07 18:26:16 [POST][id32] voting to vote_approval, preferences = [9 1 2 8 6 10 3 4 5 7], vote succesful
2022/11/07 18:26:16 [POST][id13] voting to vote_approval, preferences = [8 5 3 7 10 6 2 9 4 1], vote succesful
2022/11/07 18:26:16 [POST][id77] voting to vote_approval, preferences = [7 8 9 1 6 2 4 10 3 5], vote succesful
2022/11/07 18:26:16 [POST][id05] voting to vote_approval, preferences = [7 1 2 9 3 8 10 6 4 5], vote succesful
2022/11/07 18:26:16 [POST][id69] voting to vote_approval, preferences = [6 5 1 9 10 7 2 3 4 8], vote succesful
2022/11/07 18:26:16 [POST][id90] voting to vote_approval, preferences = [6 2 10 3 4 1 8 5 9 7], vote succesful
2022/11/07 18:26:16 [POST][id79] voting to vote_approval, preferences = [9 10 6 3 4 5 2 7 1 8], vote succesful
2022/11/07 18:26:16 [POST][id72] voting to vote_majority, preferences = [4 8 2 10 5 1 9 6 3 7], vote succesful
2022/11/07 18:26:16 [POST][id58] voting to vote_approval, preferences = [6 1 7 2 5 3 10 9 4 8], vote succesful
2022/11/07 18:26:16 [POST][id98] voting to vote_majority, preferences = [8 4 9 3 2 10 1 5 6 7], vote succesful
2022/11/07 18:26:16 [POST][id62] voting to vote_majority, preferences = [7 9 1 8 3 5 6 2 10 4], vote succesful
2022/11/07 18:26:16 [POST][id56] voting to vote_approval, preferences = [9 8 3 4 6 1 5 7 2 10], vote succesful
2022/11/07 18:26:16 [POST][id06] voting to vote_majority, preferences = [6 7 4 5 10 2 9 1 8 3], vote succesful
2022/11/07 18:26:16 [POST][id51] voting to vote_majority, preferences = [1 8 3 9 6 10 4 7 2 5], vote succesful
2022/11/07 18:26:16 [POST][id60] voting to vote_approval, preferences = [10 1 7 4 6 3 5 8 2 9], vote succesful
2022/11/07 18:26:16 [POST][id91] voting to vote_majority, preferences = [6 2 5 10 8 1 4 9 3 7], vote succesful
2022/11/07 18:26:16 [POST][id66] voting to vote_majority, preferences = [3 9 5 1 7 6 4 10 2 8], vote succesful
2022/11/07 18:26:16 [POST][id55] voting to vote_approval, preferences = [3 10 1 9 7 2 8 4 6 5], vote succesful
2022/11/07 18:26:16 [POST][id75] voting to vote_approval, preferences = [4 10 8 7 1 3 9 2 6 5], vote succesful
2022/11/07 18:26:16 [POST][id95] voting to vote_majority, preferences = [7 4 9 8 3 1 10 6 5 2], vote succesful
2022/11/07 18:26:16 [POST][id63] voting to vote_approval, preferences = [5 3 2 7 9 6 1 8 10 4], vote succesful
2022/11/07 18:26:16 [POST][id44] voting to vote_majority, preferences = [1 9 2 4 3 7 10 8 5 6], vote succesful
2022/11/07 18:26:16 [POST][id87] voting to vote_majority, preferences = [2 4 10 3 8 9 5 6 1 7], vote succesful
2022/11/07 18:26:16 [POST][id36] voting to vote_majority, preferences = [2 4 5 7 10 3 9 6 8 1], vote succesful
2022/11/07 18:26:16 [POST][id11] voting to vote_majority, preferences = [9 2 10 8 7 6 3 1 5 4], vote succesful
2022/11/07 18:26:16 [POST][id17] voting to vote_approval, preferences = [8 9 7 3 2 10 5 1 4 6], vote succesful
2022/11/07 18:26:16 [POST][id14] voting to vote_approval, preferences = [3 2 6 7 10 5 1 8 4 9], vote succesful
2022/11/07 18:26:16 [POST][id39] voting to vote_majority, preferences = [3 7 9 2 8 4 10 1 5 6], vote succesful
2022/11/07 18:26:16 [POST][id89] voting to vote_approval, preferences = [3 10 5 1 6 8 9 7 4 2], vote succesful
2022/11/07 18:26:16 [POST][id86] voting to vote_approval, preferences = [9 3 7 6 1 10 5 8 2 4], vote succesful
2022/11/07 18:26:16 [POST][id43] voting to vote_approval, preferences = [5 9 4 7 6 1 2 3 8 10], vote succesful
2022/11/07 18:26:16 [POST][id78] voting to vote_approval, preferences = [1 4 3 6 10 7 2 8 5 9], vote succesful
2022/11/07 18:26:16 [POST][id22] voting to vote_approval, preferences = [4 9 1 7 5 10 3 6 8 2], vote succesful
2022/11/07 18:26:16 [POST][id94] voting to vote_stv, preferences = [9 3 5 4 6 7 2 10 1 8], vote succesful
2022/11/07 18:26:16 [POST][id15] voting to vote_majority, preferences = [6 1 2 9 10 3 5 8 7 4], vote succesful
2022/11/07 18:26:16 [POST][id93] voting to vote_approval, preferences = [6 10 2 3 5 1 9 8 4 7], vote succesful
2022/11/07 18:26:16 [POST][id82] voting to vote_kemeny, preferences = [7 4 5 1 10 8 6 3 9 2], vote succesful
2022/11/07 18:26:16 [POST][id57] voting to vote_approval, preferences = [7 3 2 1 5 4 6 9 10 8], vote succesful
2022/11/07 18:26:16 [POST][id40] voting to vote_kemeny, preferences = [3 2 6 5 10 9 8 1 4 7], vote succesful
2022/11/07 18:26:16 [POST][id29] voting to vote_approval, preferences = [3 6 7 4 8 5 2 10 1 9], vote succesful
2022/11/07 18:26:16 [POST][id64] voting to vote_approval, preferences = [9 5 3 2 6 10 7 4 1 8], vote succesful
2022/11/07 18:26:16 [POST][id59] voting to vote_approval, preferences = [2 4 7 10 6 8 9 3 5 1], vote succesful
2022/11/07 18:26:16 [POST][id96] voting to vote_approval, preferences = [1 8 9 7 4 2 6 3 10 5], vote succesful
2022/11/07 18:26:16 [POST][id53] voting to vote_stv, preferences = [10 2 8 5 9 4 3 6 7 1], vote succesful
2022/11/07 18:26:16 [POST][id24] voting to vote_approval, preferences = [8 4 3 7 6 10 1 9 5 2], vote succesful
2022/11/07 18:26:16 [POST][id80] voting to vote_approval, preferences = [7 10 4 2 3 6 5 9 1 8], vote succesful
2022/11/07 18:26:16 [POST][id02] voting to vote_stv, preferences = [3 1 5 4 9 2 6 8 10 7], vote succesful
2022/11/07 18:26:16 [POST][id03] voting to vote_stv, preferences = [1 5 3 6 10 8 4 2 7 9], vote succesful
2022/11/07 18:26:16 [POST][id52] voting to vote_approval, preferences = [7 5 10 9 1 3 2 8 6 4], vote succesful
2022/11/07 18:26:16 [POST][id00] voting to vote_stv, preferences = [10 5 3 7 9 1 4 2 8 6], vote succesful
2022/11/07 18:26:16 [POST][id47] voting to vote_approval, preferences = [2 9 8 7 1 10 3 4 5 6], vote succesful
2022/11/07 18:26:16 [POST][id46] voting to vote_approval, preferences = [7 5 4 2 6 1 8 3 10 9], vote succesful
2022/11/07 18:26:16 [POST][id92] voting to vote_approval, preferences = [9 6 10 2 7 4 5 3 8 1], vote succesful
2022/11/07 18:26:16 [POST][id10] voting to vote_stv, preferences = [1 8 4 9 10 3 7 6 2 5], vote succesful
2022/11/07 18:26:16 [POST][id68] voting to vote_approval, preferences = [2 1 7 8 6 4 3 9 10 5], vote succesful
2022/11/07 18:26:16 [POST][id59] voting to vote_stv, preferences = [2 4 7 10 6 8 9 3 5 1], vote succesful
2022/11/07 18:26:16 [POST][id65] voting to vote_approval, preferences = [4 5 2 8 6 7 1 10 3 9], vote succesful
2022/11/07 18:26:16 [POST][id96] voting to vote_stv, preferences = [1 8 9 7 4 2 6 3 10 5], vote succesful
2022/11/07 18:26:16 [POST][id64] voting to vote_stv, preferences = [9 5 3 2 6 10 7 4 1 8], vote succesful
2022/11/07 18:26:16 [POST][id50] voting to vote_approval, preferences = [8 3 1 4 2 10 7 5 9 6], vote succesful
2022/11/07 18:26:16 [POST][id99] voting to vote_stv, preferences = [7 9 1 8 3 6 2 10 4 5], vote succesful
2022/11/07 18:26:16 [POST][id23] voting to vote_approval, preferences = [7 2 5 1 9 10 4 6 8 3], vote succesful
2022/11/07 18:26:16 [POST][id20] voting to vote_stv, preferences = [1 2 10 6 8 4 3 9 7 5], vote succesful
2022/11/07 18:26:16 [POST][id81] voting to vote_approval, preferences = [1 7 8 4 9 2 5 10 6 3], vote succesful
2022/11/07 18:26:16 [POST][id37] voting to vote_approval, preferences = [4 3 5 9 2 6 1 7 8 10], vote succesful
2022/11/07 18:26:16 [POST][id27] voting to vote_approval, preferences = [4 2 3 5 9 10 8 7 6 1], vote succesful
2022/11/07 18:26:16 [POST][id33] voting to vote_stv, preferences = [3 4 2 10 6 1 7 8 5 9], vote succesful
2022/11/07 18:26:16 [POST][id88] voting to vote_approval, preferences = [5 10 6 9 4 3 2 8 1 7], vote succesful
2022/11/07 18:26:16 [POST][id38] voting to vote_approval, preferences = [1 6 10 8 2 5 4 7 9 3], vote succesful
2022/11/07 18:26:16 [POST][id16] voting to vote_stv, preferences = [5 2 1 10 3 9 7 8 4 6], vote succesful
2022/11/07 18:26:16 [POST][id04] voting to vote_approval, preferences = [9 8 2 6 5 1 7 3 4 10], vote succesful
2022/11/07 18:26:16 [POST][id45] voting to vote_approval, preferences = [5 3 1 10 4 2 7 6 9 8], vote succesful
2022/11/07 18:26:16 [POST][id35] voting to vote_approval, preferences = [1 3 9 8 10 2 5 7 4 6], vote succesful
2022/11/07 18:26:16 [POST][id70] voting to vote_approval, preferences = [6 1 4 3 9 7 5 8 10 2], vote succesful
2022/11/07 18:26:16 [POST][id67] voting to vote_approval, preferences = [8 1 3 5 2 10 6 4 7 9], vote succesful
2022/11/07 18:26:16 [POST][id31] voting to vote_approval, preferences = [8 7 3 9 4 10 6 2 1 5], vote succesful
2022/11/07 18:26:16 [POST][id47] voting to vote_stv, preferences = [2 9 8 7 1 10 3 4 5 6], vote succesful
2022/11/07 18:26:16 [POST][id09] voting to vote_approval, preferences = [10 3 8 7 9 6 4 5 2 1], vote succesful
2022/11/07 18:26:16 [POST][id83] voting to vote_approval, preferences = [5 7 6 2 3 9 1 4 8 10], vote succesful
2022/11/07 18:26:16 [POST][id34] voting to vote_stv, preferences = [10 1 3 7 2 5 8 6 9 4], vote succesful
2022/11/07 18:26:16 [POST][id50] voting to vote_stv, preferences = [8 3 1 4 2 10 7 5 9 6], vote succesful
2022/11/07 18:26:16 [POST][id26] voting to vote_stv, preferences = [7 3 1 10 5 2 9 8 4 6], vote succesful
2022/11/07 18:26:16 [POST][id30] voting to vote_stv, preferences = [3 9 4 1 6 5 8 7 10 2], vote succesful
2022/11/07 18:26:16 [POST][id18] voting to vote_stv, preferences = [8 6 10 3 1 9 4 7 5 2], vote succesful
2022/11/07 18:26:16 [POST][id97] voting to vote_approval, preferences = [8 9 10 3 6 2 4 5 7 1], vote succesful
2022/11/07 18:26:16 [POST][id01] voting to vote_stv, preferences = [4 1 3 6 5 10 7 2 8 9], vote succesful
2022/11/07 18:26:16 [POST][id85] voting to vote_approval, preferences = [8 4 7 6 10 2 3 1 5 9], vote succesful
2022/11/07 18:26:16 [POST][id46] voting to vote_stv, preferences = [7 5 4 2 6 1 8 3 10 9], vote succesful
2022/11/07 18:26:16 [POST][id21] voting to vote_approval, preferences = [10 4 8 1 6 7 9 2 3 5], vote succesful
2022/11/07 18:26:16 [POST][id92] voting to vote_stv, preferences = [9 6 10 2 7 4 5 3 8 1], vote succesful
2022/11/07 18:26:16 [POST][id73] voting to vote_approval, preferences = [9 2 5 7 6 1 3 4 8 10], vote succesful
2022/11/07 18:26:16 [POST][id29] voting to vote_stv, preferences = [3 6 7 4 8 5 2 10 1 9], vote succesful
2022/11/07 18:26:16 [POST][id74] voting to vote_approval, preferences = [9 8 5 10 6 7 2 1 3 4], vote succesful
2022/11/07 18:26:16 [POST][id12] voting to vote_stv, preferences = [3 4 6 7 1 2 9 10 5 8], vote succesful
2022/11/07 18:26:16 [POST][id24] voting to vote_stv, preferences = [8 4 3 7 6 10 1 9 5 2], vote succesful
2022/11/07 18:26:16 [POST][id77] voting to vote_stv, preferences = [7 8 9 1 6 2 4 10 3 5], vote succesful
2022/11/07 18:26:16 [POST][id13] voting to vote_stv, preferences = [8 5 3 7 10 6 2 9 4 1], vote succesful
2022/11/07 18:26:16 [POST][id51] voting to vote_approval, preferences = [1 8 3 9 6 10 4 7 2 5], vote succesful
2022/11/07 18:26:16 [POST][id69] voting to vote_stv, preferences = [6 5 1 9 10 7 2 3 4 8], vote succesful
2022/11/07 18:26:16 [POST][id58] voting to vote_stv, preferences = [6 1 7 2 5 3 10 9 4 8], vote succesful
2022/11/07 18:26:16 [POST][id80] voting to vote_stv, preferences = [7 10 4 2 3 6 5 9 1 8], vote succesful
2022/11/07 18:26:16 [POST][id03] voting to vote_kemeny, preferences = [1 5 3 6 10 8 4 2 7 9], vote succesful
2022/11/07 18:26:16 [POST][id22] voting to vote_stv, preferences = [4 9 1 7 5 10 3 6 8 2], vote succesful
2022/11/07 18:26:16 [POST][id54] voting to vote_approval, preferences = [5 10 6 8 9 3 7 2 4 1], vote succesful
2022/11/07 18:26:16 [POST][id62] voting to vote_approval, preferences = [7 9 1 8 3 5 6 2 10 4], vote succesful
2022/11/07 18:26:16 [POST][id60] voting to vote_stv, preferences = [10 1 7 4 6 3 5 8 2 9], vote succesful
2022/11/07 18:26:16 [POST][id42] voting to vote_approval, preferences = [8 6 5 1 10 4 2 3 9 7], vote succesful
2022/11/07 18:26:16 [POST][id01] voting to vote_kemeny, preferences = [4 1 3 6 5 10 7 2 8 9], vote succesful
2022/11/07 18:26:16 [POST][id39] voting to vote_approval, preferences = [3 7 9 2 8 4 10 1 5 6], vote succesful
2022/11/07 18:26:16 [POST][id64] voting to vote_kemeny, preferences = [9 5 3 2 6 10 7 4 1 8], vote succesful
2022/11/07 18:26:16 [POST][id88] voting to vote_stv, preferences = [5 10 6 9 4 3 2 8 1 7], vote succesful
2022/11/07 18:26:16 [POST][id83] voting to vote_stv, preferences = [5 7 6 2 3 9 1 4 8 10], vote succesful
2022/11/07 18:26:16 [POST][id92] voting to vote_kemeny, preferences = [9 6 10 2 7 4 5 3 8 1], vote succesful
2022/11/07 18:26:16 [POST][id19] voting to vote_approval, preferences = [3 1 9 8 6 7 4 10 2 5], vote succesful
2022/11/07 18:26:16 [POST][id61] voting to vote_approval, preferences = [8 2 7 3 10 5 1 9 6 4], vote succesful
2022/11/07 18:26:16 [POST][id54] voting to vote_stv, preferences = [5 10 6 8 9 3 7 2 4 1], vote succesful
2022/11/07 18:26:16 [POST][id65] voting to vote_stv, preferences = [4 5 2 8 6 7 1 10 3 9], vote succesful
2022/11/07 18:26:16 [POST][id62] voting to vote_stv, preferences = [7 9 1 8 3 5 6 2 10 4], vote succesful
2022/11/07 18:26:16 [POST][id80] voting to vote_kemeny, preferences = [7 10 4 2 3 6 5 9 1 8], vote succesful
2022/11/07 18:26:16 [POST][id60] voting to vote_kemeny, preferences = [10 1 7 4 6 3 5 8 2 9], vote succesful
2022/11/07 18:26:16 [POST][id45] voting to vote_stv, preferences = [5 3 1 10 4 2 7 6 9 8], vote succesful
2022/11/07 18:26:16 [POST][id30] voting to vote_kemeny, preferences = [3 9 4 1 6 5 8 7 10 2], vote succesful
2022/11/07 18:26:16 [POST][id93] voting to vote_stv, preferences = [6 10 2 3 5 1 9 8 4 7], vote succesful
2022/11/07 18:26:16 [POST][id10] voting to vote_kemeny, preferences = [1 8 4 9 10 3 7 6 2 5], vote succesful
2022/11/07 18:26:16 [POST][id97] voting to vote_stv, preferences = [8 9 10 3 6 2 4 5 7 1], vote succesful
2022/11/07 18:26:16 [POST][id59] voting to vote_kemeny, preferences = [2 4 7 10 6 8 9 3 5 1], vote succesful
2022/11/07 18:26:16 [POST][id63] voting to vote_stv, preferences = [5 3 2 7 9 6 1 8 10 4], vote succesful
2022/11/07 18:26:16 [POST][id22] voting to vote_kemeny, preferences = [4 9 1 7 5 10 3 6 8 2], vote succesful
2022/11/07 18:26:16 [POST][id11] voting to vote_approval, preferences = [9 2 10 8 7 6 3 1 5 4], vote succesful
2022/11/07 18:26:16 [POST][id39] voting to vote_stv, preferences = [3 7 9 2 8 4 10 1 5 6], vote succesful
2022/11/07 18:26:16 [POST][id93] voting to vote_kemeny, preferences = [6 10 2 3 5 1 9 8 4 7], vote succesful
2022/11/07 18:26:16 [POST][id75] voting to vote_stv, preferences = [4 10 8 7 1 3 9 2 6 5], vote succesful
2022/11/07 18:26:16 [POST][id70] voting to vote_stv, preferences = [6 1 4 3 9 7 5 8 10 2], vote succesful
2022/11/07 18:26:16 [POST][id65] voting to vote_kemeny, preferences = [4 5 2 8 6 7 1 10 3 9], vote succesful
2022/11/07 18:26:16 [POST][id38] voting to vote_stv, preferences = [1 6 10 8 2 5 4 7 9 3], vote succesful
2022/11/07 18:26:16 [POST][id57] voting to vote_stv, preferences = [7 3 2 1 5 4 6 9 10 8], vote succesful
2022/11/07 18:26:16 [POST][id15] voting to vote_approval, preferences = [6 1 2 9 10 3 5 8 7 4], vote succesful
2022/11/07 18:26:16 [POST][id62] voting to vote_kemeny, preferences = [7 9 1 8 3 5 6 2 10 4], vote succesful
2022/11/07 18:26:16 [POST][id90] voting to vote_stv, preferences = [6 2 10 3 4 1 8 5 9 7], vote succesful
2022/11/07 18:26:16 [POST][id98] voting to vote_approval, preferences = [8 4 9 3 2 10 1 5 6 7], vote succesful
2022/11/07 18:26:16 [POST][id44] voting to vote_approval, preferences = [1 9 2 4 3 7 10 8 5 6], vote succesful
2022/11/07 18:26:16 [POST][id87] voting to vote_approval, preferences = [2 4 10 3 8 9 5 6 1 7], vote succesful
2022/11/07 18:26:16 [POST][id58] voting to vote_kemeny, preferences = [6 1 7 2 5 3 10 9 4 8], vote succesful
2022/11/07 18:26:16 [POST][id41] voting to vote_approval, preferences = [1 5 6 7 10 2 8 9 3 4], vote succesful
2022/11/07 18:26:16 [POST][id88] voting to vote_kemeny, preferences = [5 10 6 9 4 3 2 8 1 7], vote succesful
2022/11/07 18:26:16 [POST][id54] voting to vote_kemeny, preferences = [5 10 6 8 9 3 7 2 4 1], vote succesful
2022/11/07 18:26:16 [POST][id61] voting to vote_stv, preferences = [8 2 7 3 10 5 1 9 6 4], vote succesful
2022/11/07 18:26:16 [POST][id19] voting to vote_stv, preferences = [3 1 9 8 6 7 4 10 2 5], vote succesful
2022/11/07 18:26:16 [POST][id71] voting to vote_stv, preferences = [10 4 9 7 2 1 6 5 8 3], vote succesful
2022/11/07 18:26:16 [POST][id81] voting to vote_stv, preferences = [1 7 8 4 9 2 5 10 6 3], vote succesful
2022/11/07 18:26:16 [POST][id48] voting to vote_approval, preferences = [9 6 2 1 10 7 3 5 4 8], vote succesful
2022/11/07 18:26:16 [POST][id18] voting to vote_kemeny, preferences = [8 6 10 3 1 9 4 7 5 2], vote succesful
2022/11/07 18:26:16 [POST][id00] voting to vote_kemeny, preferences = [10 5 3 7 9 1 4 2 8 6], vote succesful
2022/11/07 18:26:16 [POST][id32] voting to vote_stv, preferences = [9 1 2 8 6 10 3 4 5 7], vote succesful
2022/11/07 18:26:16 [POST][id28] voting to vote_stv, preferences = [6 7 8 4 10 1 9 3 2 5], vote succesful
2022/11/07 18:26:16 [POST][id45] voting to vote_kemeny, preferences = [5 3 1 10 4 2 7 6 9 8], vote succesful
2022/11/07 18:26:16 [POST][id20] voting to vote_kemeny, preferences = [1 2 10 6 8 4 3 9 7 5], vote succesful
2022/11/07 18:26:16 [POST][id97] voting to vote_kemeny, preferences = [8 9 10 3 6 2 4 5 7 1], vote succesful
2022/11/07 18:26:16 [POST][id96] voting to vote_kemeny, preferences = [1 8 9 7 4 2 6 3 10 5], vote succesful
2022/11/07 18:26:16 [POST][id06] voting to vote_approval, preferences = [6 7 4 5 10 2 9 1 8 3], vote succesful
2022/11/07 18:26:16 [POST][id07] voting to vote_approval, preferences = [2 8 3 10 4 1 5 9 6 7], vote succesful
2022/11/07 18:26:16 [POST][id91] voting to vote_approval, preferences = [6 2 5 10 8 1 4 9 3 7], vote succesful
2022/11/07 18:26:16 [POST][id78] voting to vote_stv, preferences = [1 4 3 6 10 7 2 8 5 9], vote succesful
2022/11/07 18:26:16 [POST][id86] voting to vote_stv, preferences = [9 3 7 6 1 10 5 8 2 4], vote succesful
2022/11/07 18:26:16 [POST][id16] voting to vote_kemeny, preferences = [5 2 1 10 3 9 7 8 4 6], vote succesful
2022/11/07 18:26:16 [POST][id36] voting to vote_approval, preferences = [2 4 5 7 10 3 9 6 8 1], vote succesful
2022/11/07 18:26:16 [POST][id47] voting to vote_kemeny, preferences = [2 9 8 7 1 10 3 4 5 6], vote succesful
2022/11/07 18:26:16 [POST][id37] voting to vote_stv, preferences = [4 3 5 9 2 6 1 7 8 10], vote succesful
2022/11/07 18:26:16 [POST][id24] voting to vote_kemeny, preferences = [8 4 3 7 6 10 1 9 5 2], vote succesful
2022/11/07 18:26:16 [POST][id42] voting to vote_stv, preferences = [8 6 5 1 10 4 2 3 9 7], vote succesful
2022/11/07 18:26:16 [POST][id04] voting to vote_stv, preferences = [9 8 2 6 5 1 7 3 4 10], vote succesful
2022/11/07 18:26:16 [POST][id89] voting to vote_stv, preferences = [3 10 5 1 6 8 9 7 4 2], vote succesful
2022/11/07 18:26:16 [POST][id50] voting to vote_kemeny, preferences = [8 3 1 4 2 10 7 5 9 6], vote succesful
2022/11/07 18:26:16 [POST][id26] voting to vote_kemeny, preferences = [7 3 1 10 5 2 9 8 4 6], vote succesful
2022/11/07 18:26:16 [POST][id31] voting to vote_stv, preferences = [8 7 3 9 4 10 6 2 1 5], vote succesful
2022/11/07 18:26:16 [POST][id53] voting to vote_kemeny, preferences = [10 2 8 5 9 4 3 6 7 1], vote succesful
2022/11/07 18:26:16 [POST][id66] voting to vote_approval, preferences = [3 9 5 1 7 6 4 10 2 8], vote succesful
2022/11/07 18:26:16 [POST][id79] voting to vote_stv, preferences = [9 10 6 3 4 5 2 7 1 8], vote succesful
2022/11/07 18:26:16 [POST][id75] voting to vote_kemeny, preferences = [4 10 8 7 1 3 9 2 6 5], vote succesful
2022/11/07 18:26:16 [POST][id38] voting to vote_kemeny, preferences = [1 6 10 8 2 5 4 7 9 3], vote succesful
2022/11/07 18:26:16 [POST][id05] voting to vote_stv, preferences = [7 1 2 9 3 8 10 6 4 5], vote succesful
2022/11/07 18:26:16 [POST][id08] voting to vote_approval, preferences = [4 6 3 10 1 5 2 8 7 9], vote succesful
2022/11/07 18:26:16 [POST][id02] voting to vote_kemeny, preferences = [3 1 5 4 9 2 6 8 10 7], vote succesful
2022/11/07 18:26:16 [POST][id67] voting to vote_stv, preferences = [8 1 3 5 2 10 6 4 7 9], vote succesful
2022/11/07 18:26:16 [POST][id68] voting to vote_stv, preferences = [2 1 7 8 6 4 3 9 10 5], vote succesful
2022/11/07 18:26:16 [POST][id43] voting to vote_stv, preferences = [5 9 4 7 6 1 2 3 8 10], vote succesful
2022/11/07 18:26:16 [POST][id25] voting to vote_stv, preferences = [6 9 4 7 2 3 8 10 1 5], vote succesful
2022/11/07 18:26:16 [POST][id34] voting to vote_kemeny, preferences = [10 1 3 7 2 5 8 6 9 4], vote succesful
2022/11/07 18:26:16 [POST][id70] voting to vote_kemeny, preferences = [6 1 4 3 9 7 5 8 10 2], vote succesful
2022/11/07 18:26:16 [POST][id99] voting to vote_kemeny, preferences = [7 9 1 8 3 6 2 10 4 5], vote succesful
2022/11/07 18:26:16 [POST][id98] voting to vote_stv, preferences = [8 4 9 3 2 10 1 5 6 7], vote succesful
2022/11/07 18:26:16 [POST][id09] voting to vote_stv, preferences = [10 3 8 7 9 6 4 5 2 1], vote succesful
2022/11/07 18:26:16 [POST][id15] voting to vote_stv, preferences = [6 1 2 9 10 3 5 8 7 4], vote succesful
2022/11/07 18:26:16 [POST][id14] voting to vote_stv, preferences = [3 2 6 7 10 5 1 8 4 9], vote succesful
2022/11/07 18:26:16 [POST][id44] voting to vote_stv, preferences = [1 9 2 4 3 7 10 8 5 6], vote succesful
2022/11/07 18:26:16 [POST][id46] voting to vote_kemeny, preferences = [7 5 4 2 6 1 8 3 10 9], vote succesful
2022/11/07 18:26:16 [POST][id85] voting to vote_stv, preferences = [8 4 7 6 10 2 3 1 5 9], vote succesful
2022/11/07 18:26:16 [POST][id87] voting to vote_stv, preferences = [2 4 10 3 8 9 5 6 1 7], vote succesful
2022/11/07 18:26:16 [POST][id90] voting to vote_kemeny, preferences = [6 2 10 3 4 1 8 5 9 7], vote succesful
2022/11/07 18:26:16 [POST][id41] voting to vote_stv, preferences = [1 5 6 7 10 2 8 9 3 4], vote succesful
2022/11/07 18:26:16 [POST][id21] voting to vote_stv, preferences = [10 4 8 1 6 7 9 2 3 5], vote succesful
2022/11/07 18:26:16 [POST][id29] voting to vote_kemeny, preferences = [3 6 7 4 8 5 2 10 1 9], vote succesful
2022/11/07 18:26:16 [POST][id74] voting to vote_stv, preferences = [9 8 5 10 6 7 2 1 3 4], vote succesful
2022/11/07 18:26:16 [POST][id94] voting to vote_kemeny, preferences = [9 3 5 4 6 7 2 10 1 8], vote succesful
2022/11/07 18:26:16 [POST][id13] voting to vote_kemeny, preferences = [8 5 3 7 10 6 2 9 4 1], vote succesful
2022/11/07 18:26:16 [POST][id55] voting to vote_stv, preferences = [3 10 1 9 7 2 8 4 6 5], vote succesful
2022/11/07 18:26:16 [POST][id84] voting to vote_approval, preferences = [4 3 9 2 8 7 1 5 10 6], vote succesful
2022/11/07 18:26:16 [POST][id95] voting to vote_approval, preferences = [7 4 9 8 3 1 10 6 5 2], vote succesful
2022/11/07 18:26:16 [POST][id52] voting to vote_stv, preferences = [7 5 10 9 1 3 2 8 6 4], vote succesful
2022/11/07 18:26:16 [POST][id56] voting to vote_stv, preferences = [9 8 3 4 6 1 5 7 2 10], vote succesful
2022/11/07 18:26:16 [POST][id17] voting to vote_stv, preferences = [8 9 7 3 2 10 5 1 4 6], vote succesful
2022/11/07 18:26:16 [POST][id76] voting to vote_stv, preferences = [7 9 5 2 3 6 4 8 1 10], vote succesful
2022/11/07 18:26:16 [POST][id23] voting to vote_stv, preferences = [7 2 5 1 9 10 4 6 8 3], vote succesful
2022/11/07 18:26:16 [POST][id27] voting to vote_stv, preferences = [4 2 3 5 9 10 8 7 6 1], vote succesful
2022/11/07 18:26:16 [POST][id33] voting to vote_kemeny, preferences = [3 4 2 10 6 1 7 8 5 9], vote succesful
2022/11/07 18:26:16 [POST][id35] voting to vote_stv, preferences = [1 3 9 8 10 2 5 7 4 6], vote succesful
2022/11/07 18:26:16 [POST][id72] voting to vote_approval, preferences = [4 8 2 10 5 1 9 6 3 7], vote succesful
2022/11/07 18:26:16 [POST][id51] voting to vote_stv, preferences = [1 8 3 9 6 10 4 7 2 5], vote succesful
2022/11/07 18:26:16 [POST][id12] voting to vote_kemeny, preferences = [3 4 6 7 1 2 9 10 5 8], vote succesful
2022/11/07 18:26:16 [POST][id73] voting to vote_stv, preferences = [9 2 5 7 6 1 3 4 8 10], vote succesful
2022/11/07 18:26:16 [POST][id77] voting to vote_kemeny, preferences = [7 8 9 1 6 2 4 10 3 5], vote succesful
2022/11/07 18:26:16 [POST][id39] voting to vote_kemeny, preferences = [3 7 9 2 8 4 10 1 5 6], vote succesful
2022/11/07 18:26:16 [POST][id83] voting to vote_kemeny, preferences = [5 7 6 2 3 9 1 4 8 10], vote succesful
2022/11/07 18:26:16 [POST][id57] voting to vote_kemeny, preferences = [7 3 2 1 5 4 6 9 10 8], vote succesful
2022/11/07 18:26:16 [POST][id28] voting to vote_kemeny, preferences = [6 7 8 4 10 1 9 3 2 5], vote succesful
2022/11/07 18:26:16 [POST][id61] voting to vote_kemeny, preferences = [8 2 7 3 10 5 1 9 6 4], vote succesful

2022/11/07 18:26:16 [POST][id32] voting to vote_kemeny, preferences = [9 1 2 8 6 10 3 4 5 7], vote succesful
2022/11/07 18:26:16 [POST][id63] voting to vote_kemeny, preferences = [5 3 2 7 9 6 1 8 10 4], vote succesful
2022/11/07 18:26:16 [POST][id69] voting to vote_kemeny, preferences = [6 5 1 9 10 7 2 3 4 8], vote succesful
2022/11/07 18:26:16 [POST][id11] voting to vote_stv, preferences = [9 2 10 8 7 6 3 1 5 4], vote succesful
2022/11/07 18:26:16 [POST][id71] voting to vote_kemeny, preferences = [10 4 9 7 2 1 6 5 8 3], vote succesful
2022/11/07 18:26:16 [POST][id48] voting to vote_stv, preferences = [9 6 2 1 10 7 3 5 4 8], vote succesful
2022/11/07 18:26:16 [POST][id19] voting to vote_kemeny, preferences = [3 1 9 8 6 7 4 10 2 5], vote succesful
2022/11/07 18:26:16 [POST][id06] voting to vote_stv, preferences = [6 7 4 5 10 2 9 1 8 3], vote succesful
2022/11/07 18:26:16 [POST][id81] voting to vote_kemeny, preferences = [1 7 8 4 9 2 5 10 6 3], vote succesful
2022/11/07 18:26:16 [POST][id07] voting to vote_stv, preferences = [2 8 3 10 4 1 5 9 6 7], vote succesful
2022/11/07 18:26:16 [POST][id91] voting to vote_stv, preferences = [6 2 5 10 8 1 4 9 3 7], vote succesful
2022/11/07 18:26:16 [POST][id89] voting to vote_kemeny, preferences = [3 10 5 1 6 8 9 7 4 2], vote succesful
2022/11/07 18:26:16 [POST][id78] voting to vote_kemeny, preferences = [1 4 3 6 10 7 2 8 5 9], vote succesful
2022/11/07 18:26:16 [POST][id05] voting to vote_kemeny, preferences = [7 1 2 9 3 8 10 6 4 5], vote succesful
2022/11/07 18:26:16 [POST][id04] voting to vote_kemeny, preferences = [9 8 2 6 5 1 7 3 4 10], vote succesful
2022/11/07 18:26:16 [POST][id25] voting to vote_kemeny, preferences = [6 9 4 7 2 3 8 10 1 5], vote succesful
2022/11/07 18:26:16 [POST][id86] voting to vote_kemeny, preferences = [9 3 7 6 1 10 5 8 2 4], vote succesful
2022/11/07 18:26:16 [POST][id79] voting to vote_kemeny, preferences = [9 10 6 3 4 5 2 7 1 8], vote succesful
2022/11/07 18:26:16 [POST][id42] voting to vote_kemeny, preferences = [8 6 5 1 10 4 2 3 9 7], vote succesful
2022/11/07 18:26:16 [POST][id43] voting to vote_kemeny, preferences = [5 9 4 7 6 1 2 3 8 10], vote succesful
2022/11/07 18:26:16 [POST][id36] voting to vote_stv, preferences = [2 4 5 7 10 3 9 6 8 1], vote succesful
2022/11/07 18:26:16 [POST][id66] voting to vote_stv, preferences = [3 9 5 1 7 6 4 10 2 8], vote succesful
2022/11/07 18:26:16 [POST][id31] voting to vote_kemeny, preferences = [8 7 3 9 4 10 6 2 1 5], vote succesful
2022/11/07 18:26:16 [POST][id08] voting to vote_stv, preferences = [4 6 3 10 1 5 2 8 7 9], vote succesful
2022/11/07 18:26:16 [POST][id67] voting to vote_kemeny, preferences = [8 1 3 5 2 10 6 4 7 9], vote succesful
2022/11/07 18:26:16 [POST][id68] voting to vote_kemeny, preferences = [2 1 7 8 6 4 3 9 10 5], vote succesful
2022/11/07 18:26:16 [POST][id37] voting to vote_kemeny, preferences = [4 3 5 9 2 6 1 7 8 10], vote succesful
2022/11/07 18:26:16 [POST][id09] voting to vote_kemeny, preferences = [10 3 8 7 9 6 4 5 2 1], vote succesful
2022/11/07 18:26:16 [POST][id98] voting to vote_kemeny, preferences = [8 4 9 3 2 10 1 5 6 7], vote succesful
2022/11/07 18:26:16 [POST][id76] voting to vote_kemeny, preferences = [7 9 5 2 3 6 4 8 1 10], vote succesful
2022/11/07 18:26:16 [POST][id14] voting to vote_kemeny, preferences = [3 2 6 7 10 5 1 8 4 9], vote succesful
2022/11/07 18:26:16 [POST][id41] voting to vote_kemeny, preferences = [1 5 6 7 10 2 8 9 3 4], vote succesful
2022/11/07 18:26:16 [POST][id95] voting to vote_stv, preferences = [7 4 9 8 3 1 10 6 5 2], vote succesful
2022/11/07 18:26:16 [POST][id21] voting to vote_kemeny, preferences = [10 4 8 1 6 7 9 2 3 5], vote succesful
2022/11/07 18:26:16 [POST][id52] voting to vote_kemeny, preferences = [7 5 10 9 1 3 2 8 6 4], vote succesful
2022/11/07 18:26:16 [POST][id44] voting to vote_kemeny, preferences = [1 9 2 4 3 7 10 8 5 6], vote succesful
2022/11/07 18:26:16 [POST][id56] voting to vote_kemeny, preferences = [9 8 3 4 6 1 5 7 2 10], vote succesful
2022/11/07 18:26:16 [POST][id85] voting to vote_kemeny, preferences = [8 4 7 6 10 2 3 1 5 9], vote succesful
2022/11/07 18:26:16 [POST][id15] voting to vote_kemeny, preferences = [6 1 2 9 10 3 5 8 7 4], vote succesful
2022/11/07 18:26:16 [POST][id87] voting to vote_kemeny, preferences = [2 4 10 3 8 9 5 6 1 7], vote succesful
2022/11/07 18:26:16 [POST][id23] voting to vote_kemeny, preferences = [7 2 5 1 9 10 4 6 8 3], vote succesful
2022/11/07 18:26:16 [POST][id27] voting to vote_kemeny, preferences = [4 2 3 5 9 10 8 7 6 1], vote succesful
2022/11/07 18:26:16 [POST][id55] voting to vote_kemeny, preferences = [3 10 1 9 7 2 8 4 6 5], vote succesful
2022/11/07 18:26:16 [POST][id35] voting to vote_kemeny, preferences = [1 3 9 8 10 2 5 7 4 6], vote succesful
2022/11/07 18:26:16 [POST][id72] voting to vote_stv, preferences = [4 8 2 10 5 1 9 6 3 7], vote succesful
2022/11/07 18:26:16 [POST][id51] voting to vote_kemeny, preferences = [1 8 3 9 6 10 4 7 2 5], vote succesful
2022/11/07 18:26:16 [POST][id84] voting to vote_stv, preferences = [4 3 9 2 8 7 1 5 10 6], vote succesful
2022/11/07 18:26:16 [POST][id73] voting to vote_kemeny, preferences = [9 2 5 7 6 1 3 4 8 10], vote succesful
2022/11/07 18:26:16 [POST][id74] voting to vote_kemeny, preferences = [9 8 5 10 6 7 2 1 3 4], vote succesful
2022/11/07 18:26:16 [POST][id17] voting to vote_kemeny, preferences = [8 9 7 3 2 10 5 1 4 6], vote succesful
2022/11/07 18:26:16 [POST][id11] voting to vote_kemeny, preferences = [9 2 10 8 7 6 3 1 5 4], vote succesful
2022/11/07 18:26:16 [POST][id48] voting to vote_kemeny, preferences = [9 6 2 1 10 7 3 5 4 8], vote succesful
2022/11/07 18:26:16 [POST][id91] voting to vote_kemeny, preferences = [6 2 5 10 8 1 4 9 3 7], vote succesful
2022/11/07 18:26:16 [POST][id07] voting to vote_kemeny, preferences = [2 8 3 10 4 1 5 9 6 7], vote succesful
2022/11/07 18:26:16 [POST][id06] voting to vote_kemeny, preferences = [6 7 4 5 10 2 9 1 8 3], vote succesful
2022/11/07 18:26:16 [POST][id66] voting to vote_kemeny, preferences = [3 9 5 1 7 6 4 10 2 8], vote succesful
2022/11/07 18:26:16 [POST][id08] voting to vote_kemeny, preferences = [4 6 3 10 1 5 2 8 7 9], vote succesful
2022/11/07 18:26:16 [POST][id36] voting to vote_kemeny, preferences = [2 4 5 7 10 3 9 6 8 1], vote succesful
2022/11/07 18:26:16 [POST][id95] voting to vote_kemeny, preferences = [7 4 9 8 3 1 10 6 5 2], vote succesful
2022/11/07 18:26:16 [POST][id84] voting to vote_kemeny, preferences = [4 3 9 2 8 7 1 5 10 6], vote succesful
2022/11/07 18:26:16 [POST][id72] voting to vote_kemeny, preferences = [4 8 2 10 5 1 9 6 3 7], vote succesful
2022/11/07 18:26:21 [vote_kemeny] vote closed
2022/11/07 18:26:21 [vote_kemeny] result calculation started using KEMENY SCF
2022/11/07 18:26:21 [vote_kemeny] result calculation, best alternative after tie break = 4
2022/11/07 18:26:21 [vote_kemeny] result calculation completed, best alternative after tie break = 4
2022/11/07 18:26:21 [vote_kemeny] result open for access
2022/11/07 18:26:21 [vote_borda] vote closed
2022/11/07 18:26:21 [vote_borda] result calculation started using BORDA SCF
2022/11/07 18:26:21 [vote_borda] result calculation, best alternatives = [3]
2022/11/07 18:26:21 [vote_borda] result calculation, best alternative after tie break = 3
2022/11/07 18:26:21 [vote_borda] result calculation completed, best alternative after tie break = 3
2022/11/07 18:26:21 [vote_borda] result open for access
2022/11/07 18:26:21 [vote_majority] vote closed
2022/11/07 18:26:21 [vote_majority] result calculation started using MAJORITY SCF
2022/11/07 18:26:21 [vote_majority] result calculation, best alternatives = [7 8]
2022/11/07 18:26:21 [vote_majority] result calculation, best alternative after tie break = 7
2022/11/07 18:26:21 [vote_majority] result calculation completed, best alternative after tie break = 7
2022/11/07 18:26:21 [vote_majority] result open for access
2022/11/07 18:26:21 [vote_approval] vote closed
2022/11/07 18:26:21 [vote_approval] result calculation started using APPROVAL SCF
2022/11/07 18:26:21 [vote_approval] result calculation, seuils = [2 2 9 3 4 1 0 8 7 3 6 1 9 9 7 0 6 2 0 7 3 3 1 8 4 4 4 8 8 5 1 2 6 4 3 9 0 3 9 5 3 9 7 0 2 7 0 8 4 4 3 3 7 4 0 2 2 1 4 4 2 3 0 4 2 2 5 1 7 6 5 2 1 1 4 8 1 0 3 7 7 3 9 3 9 5 5 9 5 7 4 0 2 1 1 2 3 2 3 5]
2022/11/07 18:26:21 [vote_approval] result calculation, best alternatives = [3]
2022/11/07 18:26:21 [vote_approval] result calculation, best alternative after tie break = 3
2022/11/07 18:26:21 [vote_approval] result calculation completed, best alternative after tie break = 3
2022/11/07 18:26:21 [vote_approval] result open for access
2022/11/07 18:26:21 [vote_stv] vote closed
2022/11/07 18:26:21 [vote_stv] result calculation started using STV SCF
2022/11/07 18:26:21 [vote_stv] result calculation, best alternatives = [3]
2022/11/07 18:26:21 [vote_stv] result calculation, best alternative after tie break = 3
2022/11/07 18:26:21 [vote_stv] result calculation completed, best alternative after tie break = 3
2022/11/07 18:26:21 [vote_stv] result open for access
2022/11/07 18:26:26 [GET][id49] requesting result of election [vote_borda], Alternative elected = 3
2022/11/07 18:26:26 [GET][id49] requesting result of election [vote_majority], Alternative elected = 7
2022/11/07 18:26:26 [GET][id49] requesting result of election [vote_approval], Alternative elected = 3
2022/11/07 18:26:26 [GET][id49] requesting result of election [vote_stv], Alternative elected = 3
2022/11/07 18:26:26 [GET][id49] requesting result of election [vote_kemeny], Alternative elected = 4
2022/11/07 18:26:26 [GET][id40] requesting result of election [vote_borda], Alternative elected = 3
2022/11/07 18:26:26 [GET][id82] requesting result of election [vote_borda], Alternative elected = 3
2022/11/07 18:26:26 [GET][id40] requesting result of election [vote_majority], Alternative elected = 7
2022/11/07 18:26:26 [GET][id82] requesting result of election [vote_majority], Alternative elected = 7
2022/11/07 18:26:26 [GET][id40] requesting result of election [vote_approval], Alternative elected = 3
2022/11/07 18:26:26 [GET][id82] requesting result of election [vote_approval], Alternative elected = 3
2022/11/07 18:26:26 [GET][id40] requesting result of election [vote_stv], Alternative elected = 3
2022/11/07 18:26:26 [GET][id82] requesting result of election [vote_stv], Alternative elected = 3
2022/11/07 18:26:26 [GET][id82] requesting result of election [vote_kemeny], Alternative elected = 4
2022/11/07 18:26:26 [GET][id40] requesting result of election [vote_kemeny], Alternative elected = 4
2022/11/07 18:26:26 [GET][id03] requesting result of election [vote_borda], Alternative elected = 3
2022/11/07 18:26:26 [GET][id03] requesting result of election [vote_majority], Alternative elected = 7
2022/11/07 18:26:26 [GET][id03] requesting result of election [vote_approval], Alternative elected = 3
2022/11/07 18:26:26 [GET][id03] requesting result of election [vote_stv], Alternative elected = 3
2022/11/07 18:26:26 [GET][id03] requesting result of election [vote_kemeny], Alternative elected = 4
2022/11/07 18:26:26 [GET][id01] requesting result of election [vote_borda], Alternative elected = 3
2022/11/07 18:26:26 [GET][id64] requesting result of election [vote_borda], Alternative elected = 3
2022/11/07 18:26:26 [GET][id01] requesting result of election [vote_majority], Alternative elected = 7
2022/11/07 18:26:26 [GET][id64] requesting result of election [vote_majority], Alternative elected = 7
2022/11/07 18:26:26 [GET][id01] requesting result of election [vote_approval], Alternative elected = 3
2022/11/07 18:26:26 [GET][id60] requesting result of election [vote_borda], Alternative elected = 3
2022/11/07 18:26:26 [GET][id10] requesting result of election [vote_borda], Alternative elected = 3
2022/11/07 18:26:26 [GET][id80] requesting result of election [vote_borda], Alternative elected = 3
2022/11/07 18:26:26 [GET][id64] requesting result of election [vote_approval], Alternative elected = 3
2022/11/07 18:26:26 [GET][id59] requesting result of election [vote_borda], Alternative elected = 3
2022/11/07 18:26:26 [GET][id30] requesting result of election [vote_borda], Alternative elected = 3
2022/11/07 18:26:26 [GET][id92] requesting result of election [vote_borda], Alternative elected = 3
2022/11/07 18:26:26 [GET][id54] requesting result of election [vote_borda], Alternative elected = 3
2022/11/07 18:26:26 [GET][id22] requesting result of election [vote_borda], Alternative elected = 3
2022/11/07 18:26:26 [GET][id88] requesting result of election [vote_borda], Alternative elected = 3
2022/11/07 18:26:26 [GET][id58] requesting result of election [vote_borda], Alternative elected = 3
2022/11/07 18:26:26 [GET][id01] requesting result of election [vote_stv], Alternative elected = 3
2022/11/07 18:26:26 [GET][id65] requesting result of election [vote_borda], Alternative elected = 3
2022/11/07 18:26:26 [GET][id93] requesting result of election [vote_borda], Alternative elected = 3
2022/11/07 18:26:26 [GET][id62] requesting result of election [vote_borda], Alternative elected = 3
2022/11/07 18:26:26 [GET][id60] requesting result of election [vote_majority], Alternative elected = 7
2022/11/07 18:26:26 [GET][id10] requesting result of election [vote_majority], Alternative elected = 7
2022/11/07 18:26:26 [GET][id64] requesting result of election [vote_stv], Alternative elected = 3
2022/11/07 18:26:26 [GET][id92] requesting result of election [vote_majority], Alternative elected = 7
2022/11/07 18:26:26 [GET][id80] requesting result of election [vote_majority], Alternative elected = 7
2022/11/07 18:26:26 [GET][id59] requesting result of election [vote_majority], Alternative elected = 7
2022/11/07 18:26:26 [GET][id54] requesting result of election [vote_majority], Alternative elected = 7
2022/11/07 18:26:26 [GET][id58] requesting result of election [vote_majority], Alternative elected = 7
2022/11/07 18:26:26 [GET][id60] requesting result of election [vote_approval], Alternative elected = 3
2022/11/07 18:26:26 [GET][id20] requesting result of election [vote_borda], Alternative elected = 3
2022/11/07 18:26:26 [GET][id01] requesting result of election [vote_kemeny], Alternative elected = 4
2022/11/07 18:26:26 [GET][id00] requesting result of election [vote_borda], Alternative elected = 3
2022/11/07 18:26:26 [GET][id88] requesting result of election [vote_majority], Alternative elected = 7
2022/11/07 18:26:26 [GET][id30] requesting result of election [vote_majority], Alternative elected = 7
2022/11/07 18:26:26 [GET][id93] requesting result of election [vote_majority], Alternative elected = 7
2022/11/07 18:26:26 [GET][id65] requesting result of election [vote_majority], Alternative elected = 7
2022/11/07 18:26:26 [GET][id54] requesting result of election [vote_approval], Alternative elected = 3
2022/11/07 18:26:26 [GET][id24] requesting result of election [vote_borda], Alternative elected = 3
2022/11/07 18:26:26 [GET][id58] requesting result of election [vote_approval], Alternative elected = 3
2022/11/07 18:26:26 [GET][id60] requesting result of election [vote_stv], Alternative elected = 3
2022/11/07 18:26:26 [GET][id20] requesting result of election [vote_majority], Alternative elected = 7
2022/11/07 18:26:26 [GET][id96] requesting result of election [vote_borda], Alternative elected = 3
2022/11/07 18:26:26 [GET][id16] requesting result of election [vote_borda], Alternative elected = 3
2022/11/07 18:26:26 [GET][id45] requesting result of election [vote_borda], Alternative elected = 3
2022/11/07 18:26:26 [GET][id18] requesting result of election [vote_borda], Alternative elected = 3
2022/11/07 18:26:26 [GET][id22] requesting result of election [vote_majority], Alternative elected = 7
2022/11/07 18:26:26 [GET][id64] requesting result of election [vote_kemeny], Alternative elected = 4
2022/11/07 18:26:26 [GET][id62] requesting result of election [vote_majority], Alternative elected = 7
2022/11/07 18:26:26 [GET][id30] requesting result of election [vote_approval], Alternative elected = 3
2022/11/07 18:26:26 [GET][id10] requesting result of election [vote_approval], Alternative elected = 3
2022/11/07 18:26:26 [GET][id58] requesting result of election [vote_stv], Alternative elected = 3
2022/11/07 18:26:26 [GET][id93] requesting result of election [vote_approval], Alternative elected = 3
2022/11/07 18:26:26 [GET][id97] requesting result of election [vote_borda], Alternative elected = 3
2022/11/07 18:26:26 [GET][id80] requesting result of election [vote_approval], Alternative elected = 3
2022/11/07 18:26:26 [GET][id34] requesting result of election [vote_borda], Alternative elected = 3
2022/11/07 18:26:26 [GET][id92] requesting result of election [vote_approval], Alternative elected = 3
2022/11/07 18:26:26 [GET][id38] requesting result of election [vote_borda], Alternative elected = 3
2022/11/07 18:26:26 [GET][id70] requesting result of election [vote_borda], Alternative elected = 3
2022/11/07 18:26:26 [GET][id53] requesting result of election [vote_borda], Alternative elected = 3
2022/11/07 18:26:26 [GET][id50] requesting result of election [vote_borda], Alternative elected = 3
2022/11/07 18:26:26 [GET][id00] requesting result of election [vote_majority], Alternative elected = 7
2022/11/07 18:26:26 [GET][id45] requesting result of election [vote_majority], Alternative elected = 7
2022/11/07 18:26:26 [GET][id54] requesting result of election [vote_stv], Alternative elected = 3
2022/11/07 18:26:26 [GET][id02] requesting result of election [vote_borda], Alternative elected = 3
2022/11/07 18:26:26 [GET][id59] requesting result of election [vote_approval], Alternative elected = 3
2022/11/07 18:26:26 [GET][id22] requesting result of election [vote_approval], Alternative elected = 3
2022/11/07 18:26:26 [GET][id18] requesting result of election [vote_majority], Alternative elected = 7
2022/11/07 18:26:26 [GET][id93] requesting result of election [vote_stv], Alternative elected = 3
2022/11/07 18:26:26 [GET][id90] requesting result of election [vote_borda], Alternative elected = 3
2022/11/07 18:26:26 [GET][id29] requesting result of election [vote_borda], Alternative elected = 3
2022/11/07 18:26:26 [GET][id62] requesting result of election [vote_approval], Alternative elected = 3
2022/11/07 18:26:26 [GET][id58] requesting result of election [vote_kemeny], Alternative elected = 4
2022/11/07 18:26:26 [GET][id88] requesting result of election [vote_approval], Alternative elected = 3
2022/11/07 18:26:26 [GET][id47] requesting result of election [vote_borda], Alternative elected = 3
2022/11/07 18:26:26 [GET][id26] requesting result of election [vote_borda], Alternative elected = 3
2022/11/07 18:26:26 [GET][id75] requesting result of election [vote_borda], Alternative elected = 3
2022/11/07 18:26:26 [GET][id99] requesting result of election [vote_borda], Alternative elected = 3
2022/11/07 18:26:26 [GET][id60] requesting result of election [vote_kemeny], Alternative elected = 4
2022/11/07 18:26:26 [GET][id24] requesting result of election [vote_majority], Alternative elected = 7
2022/11/07 18:26:26 [GET][id34] requesting result of election [vote_majority], Alternative elected = 7
2022/11/07 18:26:26 [GET][id16] requesting result of election [vote_majority], Alternative elected = 7
2022/11/07 18:26:26 [GET][id46] requesting result of election [vote_borda], Alternative elected = 3
2022/11/07 18:26:26 [GET][id97] requesting result of election [vote_majority], Alternative elected = 7
2022/11/07 18:26:26 [GET][id96] requesting result of election [vote_majority], Alternative elected = 7
2022/11/07 18:26:26 [GET][id70] requesting result of election [vote_majority], Alternative elected = 7
2022/11/07 18:26:26 [GET][id65] requesting result of election [vote_approval], Alternative elected = 3
2022/11/07 18:26:26 [GET][id20] requesting result of election [vote_approval], Alternative elected = 3
2022/11/07 18:26:26 [GET][id02] requesting result of election [vote_majority], Alternative elected = 7
2022/11/07 18:26:26 [GET][id10] requesting result of election [vote_stv], Alternative elected = 3
2022/11/07 18:26:26 [GET][id30] requesting result of election [vote_stv], Alternative elected = 3
2022/11/07 18:26:26 [GET][id50] requesting result of election [vote_majority], Alternative elected = 7
2022/11/07 18:26:26 [GET][id81] requesting result of election [vote_borda], Alternative elected = 3
2022/11/07 18:26:26 [GET][id18] requesting result of election [vote_approval], Alternative elected = 3
2022/11/07 18:26:26 [GET][id53] requesting result of election [vote_majority], Alternative elected = 7
2022/11/07 18:26:26 [GET][id92] requesting result of election [vote_stv], Alternative elected = 3
2022/11/07 18:26:26 [GET][id13] requesting result of election [vote_borda], Alternative elected = 3
2022/11/07 18:26:26 [GET][id38] requesting result of election [vote_majority], Alternative elected = 7
2022/11/07 18:26:26 [GET][id45] requesting result of election [vote_approval], Alternative elected = 3
2022/11/07 18:26:26 [GET][id00] requesting result of election [vote_approval], Alternative elected = 3
2022/11/07 18:26:26 [GET][id80] requesting result of election [vote_stv], Alternative elected = 3
2022/11/07 18:26:26 [GET][id33] requesting result of election [vote_borda], Alternative elected = 3
2022/11/07 18:26:26 [GET][id94] requesting result of election [vote_borda], Alternative elected = 3
2022/11/07 18:26:26 [GET][id54] requesting result of election [vote_kemeny], Alternative elected = 4
2022/11/07 18:26:26 [GET][id22] requesting result of election [vote_stv], Alternative elected = 3
2022/11/07 18:26:26 [GET][id71] requesting result of election [vote_borda], Alternative elected = 3
2022/11/07 18:26:26 [GET][id29] requesting result of election [vote_majority], Alternative elected = 7
2022/11/07 18:26:26 [GET][id59] requesting result of election [vote_stv], Alternative elected = 3
2022/11/07 18:26:26 [GET][id19] requesting result of election [vote_borda], Alternative elected = 3
2022/11/07 18:26:26 [GET][id90] requesting result of election [vote_majority], Alternative elected = 7
2022/11/07 18:26:26 [GET][id77] requesting result of election [vote_borda], Alternative elected = 3
2022/11/07 18:26:26 [GET][id62] requesting result of election [vote_stv], Alternative elected = 3
2022/11/07 18:26:26 [GET][id55] requesting result of election [vote_borda], Alternative elected = 3
2022/11/07 18:26:26 [GET][id05] requesting result of election [vote_borda], Alternative elected = 3
2022/11/07 18:26:26 [GET][id02] requesting result of election [vote_approval], Alternative elected = 3
2022/11/07 18:26:26 [GET][id99] requesting result of election [vote_majority], Alternative elected = 7
2022/11/07 18:26:26 [GET][id96] requesting result of election [vote_approval], Alternative elected = 3
2022/11/07 18:26:26 [GET][id87] requesting result of election [vote_borda], Alternative elected = 3
2022/11/07 18:26:26 [GET][id51] requesting result of election [vote_borda], Alternative elected = 3
2022/11/07 18:26:26 [GET][id07] requesting result of election [vote_borda], Alternative elected = 3
2022/11/07 18:26:26 [GET][id15] requesting result of election [vote_borda], Alternative elected = 3
2022/11/07 18:26:26 [GET][id50] requesting result of election [vote_approval], Alternative elected = 3
2022/11/07 18:26:26 [GET][id72] requesting result of election [vote_borda], Alternative elected = 3
2022/11/07 18:26:26 [GET][id77] requesting result of election [vote_majority], Alternative elected = 7
2022/11/07 18:26:26 [GET][id62] requesting result of election [vote_kemeny], Alternative elected = 4
2022/11/07 18:26:26 [GET][id27] requesting result of election [vote_borda], Alternative elected = 3
2022/11/07 18:26:26 [GET][id98] requesting result of election [vote_borda], Alternative elected = 3
2022/11/07 18:26:26 [GET][id52] requesting result of election [vote_borda], Alternative elected = 3
2022/11/07 18:26:26 [GET][id90] requesting result of election [vote_approval], Alternative elected = 3
2022/11/07 18:26:26 [GET][id23] requesting result of election [vote_borda], Alternative elected = 3
2022/11/07 18:26:26 [GET][id39] requesting result of election [vote_borda], Alternative elected = 3
2022/11/07 18:26:26 [GET][id05] requesting result of election [vote_majority], Alternative elected = 7
2022/11/07 18:26:26 [GET][id99] requesting result of election [vote_approval], Alternative elected = 3
2022/11/07 18:26:26 [GET][id02] requesting result of election [vote_stv], Alternative elected = 3
2022/11/07 18:26:26 [GET][id85] requesting result of election [vote_borda], Alternative elected = 3
2022/11/07 18:26:26 [GET][id35] requesting result of election [vote_borda], Alternative elected = 3
2022/11/07 18:26:26 [GET][id55] requesting result of election [vote_majority], Alternative elected = 7
2022/11/07 18:26:26 [GET][id61] requesting result of election [vote_borda], Alternative elected = 3
2022/11/07 18:26:26 [GET][id09] requesting result of election [vote_borda], Alternative elected = 3
2022/11/07 18:26:26 [GET][id76] requesting result of election [vote_borda], Alternative elected = 3
2022/11/07 18:26:26 [GET][id31] requesting result of election [vote_borda], Alternative elected = 3
2022/11/07 18:26:26 [GET][id79] requesting result of election [vote_borda], Alternative elected = 3
2022/11/07 18:26:26 [GET][id13] requesting result of election [vote_majority], Alternative elected = 7
2022/11/07 18:26:26 [GET][id39] requesting result of election [vote_majority], Alternative elected = 7
2022/11/07 18:26:26 [GET][id30] requesting result of election [vote_kemeny], Alternative elected = 4
2022/11/07 18:26:26 [GET][id23] requesting result of election [vote_majority], Alternative elected = 7
2022/11/07 18:26:26 [GET][id10] requesting result of election [vote_kemeny], Alternative elected = 4
2022/11/07 18:26:26 [GET][id16] requesting result of election [vote_approval], Alternative elected = 3
2022/11/07 18:26:26 [GET][id25] requesting result of election [vote_borda], Alternative elected = 3
2022/11/07 18:26:26 [GET][id93] requesting result of election [vote_kemeny], Alternative elected = 4
2022/11/07 18:26:26 [GET][id65] requesting result of election [vote_stv], Alternative elected = 3
2022/11/07 18:26:26 [GET][id50] requesting result of election [vote_stv], Alternative elected = 3
2022/11/07 18:26:26 [GET][id15] requesting result of election [vote_majority], Alternative elected = 7
2022/11/07 18:26:26 [GET][id70] requesting result of election [vote_approval], Alternative elected = 3
2022/11/07 18:26:26 [GET][id77] requesting result of election [vote_approval], Alternative elected = 3
2022/11/07 18:26:26 [GET][id85] requesting result of election [vote_majority], Alternative elected = 7
2022/11/07 18:26:26 [GET][id34] requesting result of election [vote_approval], Alternative elected = 3
2022/11/07 18:26:26 [GET][id52] requesting result of election [vote_majority], Alternative elected = 7
2022/11/07 18:26:26 [GET][id96] requesting result of election [vote_stv], Alternative elected = 3
2022/11/07 18:26:26 [GET][id91] requesting result of election [vote_borda], Alternative elected = 3
2022/11/07 18:26:26 [GET][id88] requesting result of election [vote_stv], Alternative elected = 3
2022/11/07 18:26:26 [GET][id18] requesting result of election [vote_stv], Alternative elected = 3
2022/11/07 18:26:26 [GET][id81] requesting result of election [vote_majority], Alternative elected = 7
2022/11/07 18:26:26 [GET][id87] requesting result of election [vote_majority], Alternative elected = 7
2022/11/07 18:26:26 [GET][id05] requesting result of election [vote_approval], Alternative elected = 3
2022/11/07 18:26:26 [GET][id27] requesting result of election [vote_majority], Alternative elected = 7
2022/11/07 18:26:26 [GET][id99] requesting result of election [vote_stv], Alternative elected = 3
2022/11/07 18:26:26 [GET][id85] requesting result of election [vote_approval], Alternative elected = 3
2022/11/07 18:26:26 [GET][id07] requesting result of election [vote_majority], Alternative elected = 7
2022/11/07 18:26:26 [GET][id72] requesting result of election [vote_majority], Alternative elected = 7
2022/11/07 18:26:26 [GET][id74] requesting result of election [vote_borda], Alternative elected = 3
2022/11/07 18:26:26 [GET][id44] requesting result of election [vote_borda], Alternative elected = 3
2022/11/07 18:26:26 [GET][id19] requesting result of election [vote_majority], Alternative elected = 7
2022/11/07 18:26:26 [GET][id51] requesting result of election [vote_majority], Alternative elected = 7
2022/11/07 18:26:26 [GET][id56] requesting result of election [vote_borda], Alternative elected = 3
2022/11/07 18:26:26 [GET][id27] requesting result of election [vote_approval], Alternative elected = 3
2022/11/07 18:26:26 [GET][id88] requesting result of election [vote_kemeny], Alternative elected = 4
2022/11/07 18:26:26 [GET][id18] requesting result of election [vote_kemeny], Alternative elected = 4
2022/11/07 18:26:26 [GET][id70] requesting result of election [vote_stv], Alternative elected = 3
2022/11/07 18:26:26 [GET][id02] requesting result of election [vote_kemeny], Alternative elected = 4
2022/11/07 18:26:26 [GET][id05] requesting result of election [vote_stv], Alternative elected = 3
2022/11/07 18:26:26 [GET][id91] requesting result of election [vote_majority], Alternative elected = 7
2022/11/07 18:26:26 [GET][id81] requesting result of election [vote_approval], Alternative elected = 3
2022/11/07 18:26:26 [GET][id96] requesting result of election [vote_kemeny], Alternative elected = 4
2022/11/07 18:26:26 [GET][id52] requesting result of election [vote_approval], Alternative elected = 3
2022/11/07 18:26:26 [GET][id13] requesting result of election [vote_approval], Alternative elected = 3
2022/11/07 18:26:26 [GET][id77] requesting result of election [vote_stv], Alternative elected = 3
2022/11/07 18:26:26 [GET][id44] requesting result of election [vote_majority], Alternative elected = 7
2022/11/07 18:26:26 [GET][id87] requesting result of election [vote_approval], Alternative elected = 3
2022/11/07 18:26:26 [GET][id51] requesting result of election [vote_approval], Alternative elected = 3
2022/11/07 18:26:26 [GET][id69] requesting result of election [vote_borda], Alternative elected = 3
2022/11/07 18:26:26 [GET][id79] requesting result of election [vote_majority], Alternative elected = 7
2022/11/07 18:26:26 [GET][id19] requesting result of election [vote_approval], Alternative elected = 3
2022/11/07 18:26:26 [GET][id27] requesting result of election [vote_stv], Alternative elected = 3
2022/11/07 18:26:26 [GET][id56] requesting result of election [vote_majority], Alternative elected = 7
2022/11/07 18:26:26 [GET][id12] requesting result of election [vote_borda], Alternative elected = 3
2022/11/07 18:26:26 [GET][id78] requesting result of election [vote_borda], Alternative elected = 3
2022/11/07 18:26:26 [GET][id11] requesting result of election [vote_borda], Alternative elected = 3
2022/11/07 18:26:26 [GET][id48] requesting result of election [vote_borda], Alternative elected = 3
2022/11/07 18:26:26 [GET][id99] requesting result of election [vote_kemeny], Alternative elected = 4
2022/11/07 18:26:26 [GET][id20] requesting result of election [vote_stv], Alternative elected = 3
2022/11/07 18:26:26 [GET][id17] requesting result of election [vote_borda], Alternative elected = 3
2022/11/07 18:26:26 [GET][id75] requesting result of election [vote_majority], Alternative elected = 7
2022/11/07 18:26:26 [GET][id28] requesting result of election [vote_borda], Alternative elected = 3
2022/11/07 18:26:26 [GET][id24] requesting result of election [vote_approval], Alternative elected = 3
2022/11/07 18:26:26 [GET][id66] requesting result of election [vote_borda], Alternative elected = 3
2022/11/07 18:26:26 [GET][id26] requesting result of election [vote_majority], Alternative elected = 7
2022/11/07 18:26:26 [GET][id47] requesting result of election [vote_majority], Alternative elected = 7
2022/11/07 18:26:26 [GET][id63] requesting result of election [vote_borda], Alternative elected = 3
2022/11/07 18:26:26 [GET][id95] requesting result of election [vote_borda], Alternative elected = 3
2022/11/07 18:26:26 [GET][id57] requesting result of election [vote_borda], Alternative elected = 3
2022/11/07 18:26:26 [GET][id32] requesting result of election [vote_borda], Alternative elected = 3
2022/11/07 18:26:26 [GET][id09] requesting result of election [vote_majority], Alternative elected = 7
2022/11/07 18:26:26 [GET][id86] requesting result of election [vote_borda], Alternative elected = 3
2022/11/07 18:26:26 [GET][id68] requesting result of election [vote_borda], Alternative elected = 3
2022/11/07 18:26:26 [GET][id72] requesting result of election [vote_approval], Alternative elected = 3
2022/11/07 18:26:26 [GET][id14] requesting result of election [vote_borda], Alternative elected = 3
2022/11/07 18:26:26 [GET][id04] requesting result of election [vote_borda], Alternative elected = 3
2022/11/07 18:26:26 [GET][id85] requesting result of election [vote_stv], Alternative elected = 3
2022/11/07 18:26:26 [GET][id76] requesting result of election [vote_majority], Alternative elected = 7
2022/11/07 18:26:26 [GET][id07] requesting result of election [vote_approval], Alternative elected = 3
2022/11/07 18:26:26 [GET][id83] requesting result of election [vote_borda], Alternative elected = 3
2022/11/07 18:26:26 [GET][id74] requesting result of election [vote_majority], Alternative elected = 7
2022/11/07 18:26:26 [GET][id55] requesting result of election [vote_approval], Alternative elected = 3
2022/11/07 18:26:26 [GET][id31] requesting result of election [vote_majority], Alternative elected = 7
2022/11/07 18:26:26 [GET][id70] requesting result of election [vote_kemeny], Alternative elected = 4
2022/11/07 18:26:26 [GET][id61] requesting result of election [vote_majority], Alternative elected = 7
2022/11/07 18:26:26 [GET][id06] requesting result of election [vote_borda], Alternative elected = 3
2022/11/07 18:26:26 [GET][id35] requesting result of election [vote_majority], Alternative elected = 7
2022/11/07 18:26:26 [GET][id67] requesting result of election [vote_borda], Alternative elected = 3
2022/11/07 18:26:26 [GET][id43] requesting result of election [vote_borda], Alternative elected = 3
2022/11/07 18:26:26 [GET][id37] requesting result of election [vote_borda], Alternative elected = 3
2022/11/07 18:26:26 [GET][id36] requesting result of election [vote_borda], Alternative elected = 3
2022/11/07 18:26:26 [GET][id21] requesting result of election [vote_borda], Alternative elected = 3
2022/11/07 18:26:26 [GET][id05] requesting result of election [vote_kemeny], Alternative elected = 4
2022/11/07 18:26:26 [GET][id46] requesting result of election [vote_majority], Alternative elected = 7
2022/11/07 18:26:26 [GET][id38] requesting result of election [vote_approval], Alternative elected = 3
2022/11/07 18:26:26 [GET][id84] requesting result of election [vote_borda], Alternative elected = 3
2022/11/07 18:26:26 [GET][id45] requesting result of election [vote_stv], Alternative elected = 3
2022/11/07 18:26:26 [GET][id00] requesting result of election [vote_stv], Alternative elected = 3
2022/11/07 18:26:26 [GET][id91] requesting result of election [vote_approval], Alternative elected = 3
2022/11/07 18:26:26 [GET][id89] requesting result of election [vote_borda], Alternative elected = 3
2022/11/07 18:26:26 [GET][id13] requesting result of election [vote_stv], Alternative elected = 3
2022/11/07 18:26:26 [GET][id52] requesting result of election [vote_stv], Alternative elected = 3
2022/11/07 18:26:26 [GET][id81] requesting result of election [vote_stv], Alternative elected = 3
2022/11/07 18:26:26 [GET][id77] requesting result of election [vote_kemeny], Alternative elected = 4
2022/11/07 18:26:26 [GET][id42] requesting result of election [vote_borda], Alternative elected = 3
2022/11/07 18:26:26 [GET][id90] requesting result of election [vote_stv], Alternative elected = 3
2022/11/07 18:26:26 [GET][id98] requesting result of election [vote_majority], Alternative elected = 7
2022/11/07 18:26:26 [GET][id80] requesting result of election [vote_kemeny], Alternative elected = 4
2022/11/07 18:26:26 [GET][id41] requesting result of election [vote_borda], Alternative elected = 3
2022/11/07 18:26:26 [GET][id44] requesting result of election [vote_approval], Alternative elected = 3
2022/11/07 18:26:26 [GET][id79] requesting result of election [vote_approval], Alternative elected = 3
2022/11/07 18:26:26 [GET][id92] requesting result of election [vote_kemeny], Alternative elected = 4
2022/11/07 18:26:26 [GET][id19] requesting result of election [vote_stv], Alternative elected = 3
2022/11/07 18:26:26 [GET][id69] requesting result of election [vote_majority], Alternative elected = 7
2022/11/07 18:26:26 [GET][id87] requesting result of election [vote_stv], Alternative elected = 3
2022/11/07 18:26:26 [GET][id27] requesting result of election [vote_kemeny], Alternative elected = 4
2022/11/07 18:26:26 [GET][id51] requesting result of election [vote_stv], Alternative elected = 3
2022/11/07 18:26:26 [GET][id56] requesting result of election [vote_approval], Alternative elected = 3
2022/11/07 18:26:26 [GET][id53] requesting result of election [vote_approval], Alternative elected = 3
2022/11/07 18:26:26 [GET][id11] requesting result of election [vote_majority], Alternative elected = 7
2022/11/07 18:26:26 [GET][id12] requesting result of election [vote_majority], Alternative elected = 7
2022/11/07 18:26:26 [GET][id20] requesting result of election [vote_kemeny], Alternative elected = 4
2022/11/07 18:26:26 [GET][id48] requesting result of election [vote_majority], Alternative elected = 7
2022/11/07 18:26:26 [GET][id75] requesting result of election [vote_approval], Alternative elected = 3
2022/11/07 18:26:26 [GET][id94] requesting result of election [vote_majority], Alternative elected = 7
2022/11/07 18:26:26 [GET][id24] requesting result of election [vote_stv], Alternative elected = 3
2022/11/07 18:26:26 [GET][id97] requesting result of election [vote_approval], Alternative elected = 3
2022/11/07 18:26:26 [GET][id17] requesting result of election [vote_majority], Alternative elected = 7
2022/11/07 18:26:26 [GET][id26] requesting result of election [vote_approval], Alternative elected = 3
2022/11/07 18:26:26 [GET][id66] requesting result of election [vote_majority], Alternative elected = 7
2022/11/07 18:26:26 [GET][id71] requesting result of election [vote_majority], Alternative elected = 7
2022/11/07 18:26:26 [GET][id78] requesting result of election [vote_majority], Alternative elected = 7
2022/11/07 18:26:26 [GET][id33] requesting result of election [vote_majority], Alternative elected = 7
2022/11/07 18:26:26 [GET][id47] requesting result of election [vote_approval], Alternative elected = 3
2022/11/07 18:26:26 [GET][id29] requesting result of election [vote_approval], Alternative elected = 3
2022/11/07 18:26:26 [GET][id22] requesting result of election [vote_kemeny], Alternative elected = 4
2022/11/07 18:26:26 [GET][id57] requesting result of election [vote_majority], Alternative elected = 7
2022/11/07 18:26:26 [GET][id32] requesting result of election [vote_majority], Alternative elected = 7
2022/11/07 18:26:26 [GET][id85] requesting result of election [vote_kemeny], Alternative elected = 4
2022/11/07 18:26:26 [GET][id14] requesting result of election [vote_majority], Alternative elected = 7
2022/11/07 18:26:26 [GET][id61] requesting result of election [vote_approval], Alternative elected = 3
2022/11/07 18:26:26 [GET][id67] requesting result of election [vote_majority], Alternative elected = 7
2022/11/07 18:26:26 [GET][id31] requesting result of election [vote_approval], Alternative elected = 3
2022/11/07 18:26:26 [GET][id35] requesting result of election [vote_approval], Alternative elected = 3
2022/11/07 18:26:26 [GET][id34] requesting result of election [vote_stv], Alternative elected = 3
2022/11/07 18:26:26 [GET][id39] requesting result of election [vote_approval], Alternative elected = 3
2022/11/07 18:26:26 [GET][id42] requesting result of election [vote_majority], Alternative elected = 7
2022/11/07 18:26:26 [GET][id81] requesting result of election [vote_kemeny], Alternative elected = 4
2022/11/07 18:26:26 [GET][id25] requesting result of election [vote_majority], Alternative elected = 7
2022/11/07 18:26:26 [GET][id50] requesting result of election [vote_kemeny], Alternative elected = 4
2022/11/07 18:26:26 [GET][id90] requesting result of election [vote_kemeny], Alternative elected = 4
2022/11/07 18:26:26 [GET][id98] requesting result of election [vote_approval], Alternative elected = 3
2022/11/07 18:26:26 [GET][id16] requesting result of election [vote_stv], Alternative elected = 3
2022/11/07 18:26:26 [GET][id79] requesting result of election [vote_stv], Alternative elected = 3
2022/11/07 18:26:26 [GET][id73] requesting result of election [vote_borda], Alternative elected = 3
2022/11/07 18:26:26 [GET][id07] requesting result of election [vote_stv], Alternative elected = 3
2022/11/07 18:26:26 [GET][id76] requesting result of election [vote_approval], Alternative elected = 3
2022/11/07 18:26:26 [GET][id06] requesting result of election [vote_majority], Alternative elected = 7
2022/11/07 18:26:26 [GET][id95] requesting result of election [vote_majority], Alternative elected = 7
2022/11/07 18:26:26 [GET][id15] requesting result of election [vote_approval], Alternative elected = 3
2022/11/07 18:26:26 [GET][id74] requesting result of election [vote_approval], Alternative elected = 3
2022/11/07 18:26:26 [GET][id65] requesting result of election [vote_kemeny], Alternative elected = 4
2022/11/07 18:26:26 [GET][id41] requesting result of election [vote_majority], Alternative elected = 7
2022/11/07 18:26:26 [GET][id63] requesting result of election [vote_majority], Alternative elected = 7
2022/11/07 18:26:26 [GET][id28] requesting result of election [vote_majority], Alternative elected = 7
2022/11/07 18:26:26 [GET][id83] requesting result of election [vote_majority], Alternative elected = 7
2022/11/07 18:26:26 [GET][id44] requesting result of election [vote_stv], Alternative elected = 3
2022/11/07 18:26:26 [GET][id68] requesting result of election [vote_majority], Alternative elected = 7
2022/11/07 18:26:26 [GET][id52] requesting result of election [vote_kemeny], Alternative elected = 4
2022/11/07 18:26:26 [GET][id55] requesting result of election [vote_stv], Alternative elected = 3
2022/11/07 18:26:26 [GET][id59] requesting result of election [vote_kemeny], Alternative elected = 4
2022/11/07 18:26:26 [GET][id86] requesting result of election [vote_majority], Alternative elected = 7
2022/11/07 18:26:26 [GET][id72] requesting result of election [vote_stv], Alternative elected = 3
2022/11/07 18:26:26 [GET][id29] requesting result of election [vote_stv], Alternative elected = 3
2022/11/07 18:26:26 [GET][id53] requesting result of election [vote_stv], Alternative elected = 3
2022/11/07 18:26:26 [GET][id47] requesting result of election [vote_stv], Alternative elected = 3
2022/11/07 18:26:26 [GET][id33] requesting result of election [vote_approval], Alternative elected = 3
2022/11/07 18:26:26 [GET][id04] requesting result of election [vote_majority], Alternative elected = 7
2022/11/07 18:26:26 [GET][id37] requesting result of election [vote_majority], Alternative elected = 7
2022/11/07 18:26:26 [GET][id09] requesting result of election [vote_approval], Alternative elected = 3
2022/11/07 18:26:26 [GET][id46] requesting result of election [vote_approval], Alternative elected = 3
2022/11/07 18:26:26 [GET][id21] requesting result of election [vote_majority], Alternative elected = 7
2022/11/07 18:26:26 [GET][id84] requesting result of election [vote_majority], Alternative elected = 7
2022/11/07 18:26:26 [GET][id38] requesting result of election [vote_stv], Alternative elected = 3
2022/11/07 18:26:26 [GET][id32] requesting result of election [vote_approval], Alternative elected = 3
2022/11/07 18:26:26 [GET][id14] requesting result of election [vote_approval], Alternative elected = 3
2022/11/07 18:26:26 [GET][id13] requesting result of election [vote_kemeny], Alternative elected = 4
2022/11/07 18:26:26 [GET][id23] requesting result of election [vote_approval], Alternative elected = 3
2022/11/07 18:26:26 [GET][id43] requesting result of election [vote_majority], Alternative elected = 7
2022/11/07 18:26:26 [GET][id00] requesting result of election [vote_kemeny], Alternative elected = 4
2022/11/07 18:26:26 [GET][id45] requesting result of election [vote_kemeny], Alternative elected = 4
2022/11/07 18:26:26 [GET][id91] requesting result of election [vote_stv], Alternative elected = 3
2022/11/07 18:26:26 [GET][id36] requesting result of election [vote_majority], Alternative elected = 7
2022/11/07 18:26:26 [GET][id08] requesting result of election [vote_borda], Alternative elected = 3
2022/11/07 18:26:26 [GET][id89] requesting result of election [vote_majority], Alternative elected = 7
2022/11/07 18:26:26 [GET][id69] requesting result of election [vote_approval], Alternative elected = 3
2022/11/07 18:26:26 [GET][id19] requesting result of election [vote_kemeny], Alternative elected = 4
2022/11/07 18:26:26 [GET][id51] requesting result of election [vote_kemeny], Alternative elected = 4
2022/11/07 18:26:26 [GET][id11] requesting result of election [vote_approval], Alternative elected = 3
2022/11/07 18:26:26 [GET][id87] requesting result of election [vote_kemeny], Alternative elected = 4
2022/11/07 18:26:26 [GET][id56] requesting result of election [vote_stv], Alternative elected = 3
2022/11/07 18:26:26 [GET][id12] requesting result of election [vote_approval], Alternative elected = 3
2022/11/07 18:26:26 [GET][id48] requesting result of election [vote_approval], Alternative elected = 3
2022/11/07 18:26:26 [GET][id71] requesting result of election [vote_approval], Alternative elected = 3
2022/11/07 18:26:26 [GET][id39] requesting result of election [vote_stv], Alternative elected = 3
2022/11/07 18:26:26 [GET][id97] requesting result of election [vote_stv], Alternative elected = 3
2022/11/07 18:26:26 [GET][id94] requesting result of election [vote_approval], Alternative elected = 3
2022/11/07 18:26:26 [GET][id25] requesting result of election [vote_approval], Alternative elected = 3
2022/11/07 18:26:26 [GET][id75] requesting result of election [vote_stv], Alternative elected = 3
2022/11/07 18:26:26 [GET][id17] requesting result of election [vote_approval], Alternative elected = 3
2022/11/07 18:26:26 [GET][id79] requesting result of election [vote_kemeny], Alternative elected = 4
2022/11/07 18:26:26 [GET][id66] requesting result of election [vote_approval], Alternative elected = 3
2022/11/07 18:26:26 [GET][id07] requesting result of election [vote_kemeny], Alternative elected = 4
2022/11/07 18:26:26 [GET][id24] requesting result of election [vote_kemeny], Alternative elected = 4
2022/11/07 18:26:26 [GET][id78] requesting result of election [vote_approval], Alternative elected = 3
2022/11/07 18:26:26 [GET][id26] requesting result of election [vote_stv], Alternative elected = 3
2022/11/07 18:26:26 [GET][id74] requesting result of election [vote_stv], Alternative elected = 3
2022/11/07 18:26:26 [GET][id06] requesting result of election [vote_approval], Alternative elected = 3
2022/11/07 18:26:26 [GET][id28] requesting result of election [vote_approval], Alternative elected = 3
2022/11/07 18:26:26 [GET][id63] requesting result of election [vote_approval], Alternative elected = 3
2022/11/07 18:26:26 [GET][id44] requesting result of election [vote_kemeny], Alternative elected = 4
2022/11/07 18:26:26 [GET][id41] requesting result of election [vote_approval], Alternative elected = 3
2022/11/07 18:26:26 [GET][id15] requesting result of election [vote_stv], Alternative elected = 3
2022/11/07 18:26:26 [GET][id57] requesting result of election [vote_approval], Alternative elected = 3
2022/11/07 18:26:26 [GET][id83] requesting result of election [vote_approval], Alternative elected = 3
2022/11/07 18:26:26 [GET][id72] requesting result of election [vote_kemeny], Alternative elected = 4
2022/11/07 18:26:26 [GET][id31] requesting result of election [vote_stv], Alternative elected = 3
2022/11/07 18:26:26 [GET][id55] requesting result of election [vote_kemeny], Alternative elected = 4
2022/11/07 18:26:26 [GET][id76] requesting result of election [vote_stv], Alternative elected = 3
2022/11/07 18:26:26 [GET][id67] requesting result of election [vote_approval], Alternative elected = 3
2022/11/07 18:26:26 [GET][id68] requesting result of election [vote_approval], Alternative elected = 3
2022/11/07 18:26:26 [GET][id61] requesting result of election [vote_stv], Alternative elected = 3
2022/11/07 18:26:26 [GET][id73] requesting result of election [vote_majority], Alternative elected = 7
2022/11/07 18:26:26 [GET][id29] requesting result of election [vote_kemeny], Alternative elected = 4
2022/11/07 18:26:26 [GET][id35] requesting result of election [vote_stv], Alternative elected = 3
2022/11/07 18:26:26 [GET][id33] requesting result of election [vote_stv], Alternative elected = 3
2022/11/07 18:26:26 [GET][id53] requesting result of election [vote_kemeny], Alternative elected = 4
2022/11/07 18:26:26 [GET][id47] requesting result of election [vote_kemeny], Alternative elected = 4
2022/11/07 18:26:26 [GET][id04] requesting result of election [vote_approval], Alternative elected = 3
2022/11/07 18:26:26 [GET][id86] requesting result of election [vote_approval], Alternative elected = 3
2022/11/07 18:26:26 [GET][id37] requesting result of election [vote_approval], Alternative elected = 3
2022/11/07 18:26:26 [GET][id09] requesting result of election [vote_stv], Alternative elected = 3
2022/11/07 18:26:26 [GET][id95] requesting result of election [vote_approval], Alternative elected = 3
2022/11/07 18:26:26 [GET][id21] requesting result of election [vote_approval], Alternative elected = 3
2022/11/07 18:26:26 [GET][id46] requesting result of election [vote_stv], Alternative elected = 3
2022/11/07 18:26:26 [GET][id34] requesting result of election [vote_kemeny], Alternative elected = 4
2022/11/07 18:26:26 [GET][id98] requesting result of election [vote_stv], Alternative elected = 3
2022/11/07 18:26:26 [GET][id42] requesting result of election [vote_approval], Alternative elected = 3
2022/11/07 18:26:26 [GET][id84] requesting result of election [vote_approval], Alternative elected = 3
2022/11/07 18:26:26 [GET][id38] requesting result of election [vote_kemeny], Alternative elected = 4
2022/11/07 18:26:26 [GET][id32] requesting result of election [vote_stv], Alternative elected = 3
2022/11/07 18:26:26 [GET][id14] requesting result of election [vote_stv], Alternative elected = 3
2022/11/07 18:26:26 [GET][id23] requesting result of election [vote_stv], Alternative elected = 3
2022/11/07 18:26:26 [GET][id43] requesting result of election [vote_approval], Alternative elected = 3
2022/11/07 18:26:26 [GET][id16] requesting result of election [vote_kemeny], Alternative elected = 4
2022/11/07 18:26:26 [GET][id91] requesting result of election [vote_kemeny], Alternative elected = 4
2022/11/07 18:26:26 [GET][id36] requesting result of election [vote_approval], Alternative elected = 3
2022/11/07 18:26:26 [GET][id08] requesting result of election [vote_majority], Alternative elected = 7
2022/11/07 18:26:26 [GET][id89] requesting result of election [vote_approval], Alternative elected = 3
2022/11/07 18:26:26 [GET][id69] requesting result of election [vote_stv], Alternative elected = 3
2022/11/07 18:26:26 [GET][id12] requesting result of election [vote_stv], Alternative elected = 3
2022/11/07 18:26:26 [GET][id48] requesting result of election [vote_stv], Alternative elected = 3
2022/11/07 18:26:26 [GET][id71] requesting result of election [vote_stv], Alternative elected = 3
2022/11/07 18:26:26 [GET][id11] requesting result of election [vote_stv], Alternative elected = 3
2022/11/07 18:26:26 [GET][id56] requesting result of election [vote_kemeny], Alternative elected = 4
2022/11/07 18:26:26 [GET][id75] requesting result of election [vote_kemeny], Alternative elected = 4
2022/11/07 18:26:26 [GET][id97] requesting result of election [vote_kemeny], Alternative elected = 4
2022/11/07 18:26:26 [GET][id39] requesting result of election [vote_kemeny], Alternative elected = 4
2022/11/07 18:26:26 [GET][id17] requesting result of election [vote_stv], Alternative elected = 3
2022/11/07 18:26:26 [GET][id66] requesting result of election [vote_stv], Alternative elected = 3
2022/11/07 18:26:26 [GET][id25] requesting result of election [vote_stv], Alternative elected = 3
2022/11/07 18:26:26 [GET][id94] requesting result of election [vote_stv], Alternative elected = 3
2022/11/07 18:26:26 [GET][id78] requesting result of election [vote_stv], Alternative elected = 3
2022/11/07 18:26:26 [GET][id74] requesting result of election [vote_kemeny], Alternative elected = 4
2022/11/07 18:26:26 [GET][id26] requesting result of election [vote_kemeny], Alternative elected = 4
2022/11/07 18:26:26 [GET][id06] requesting result of election [vote_stv], Alternative elected = 3
2022/11/07 18:26:26 [GET][id41] requesting result of election [vote_stv], Alternative elected = 3
2022/11/07 18:26:26 [GET][id28] requesting result of election [vote_stv], Alternative elected = 3
2022/11/07 18:26:26 [GET][id57] requesting result of election [vote_stv], Alternative elected = 3
2022/11/07 18:26:26 [GET][id83] requesting result of election [vote_stv], Alternative elected = 3
2022/11/07 18:26:26 [GET][id31] requesting result of election [vote_kemeny], Alternative elected = 4
2022/11/07 18:26:26 [GET][id63] requesting result of election [vote_stv], Alternative elected = 3
2022/11/07 18:26:26 [GET][id15] requesting result of election [vote_kemeny], Alternative elected = 4
2022/11/07 18:26:26 [GET][id76] requesting result of election [vote_kemeny], Alternative elected = 4
2022/11/07 18:26:26 [GET][id61] requesting result of election [vote_kemeny], Alternative elected = 4
2022/11/07 18:26:26 [GET][id67] requesting result of election [vote_stv], Alternative elected = 3
2022/11/07 18:26:26 [GET][id68] requesting result of election [vote_stv], Alternative elected = 3
2022/11/07 18:26:26 [GET][id73] requesting result of election [vote_approval], Alternative elected = 3
2022/11/07 18:26:26 [GET][id35] requesting result of election [vote_kemeny], Alternative elected = 4
2022/11/07 18:26:26 [GET][id33] requesting result of election [vote_kemeny], Alternative elected = 4
2022/11/07 18:26:26 [GET][id04] requesting result of election [vote_stv], Alternative elected = 3
2022/11/07 18:26:26 [GET][id37] requesting result of election [vote_stv], Alternative elected = 3
2022/11/07 18:26:26 [GET][id86] requesting result of election [vote_stv], Alternative elected = 3
2022/11/07 18:26:26 [GET][id09] requesting result of election [vote_kemeny], Alternative elected = 4
2022/11/07 18:26:26 [GET][id21] requesting result of election [vote_stv], Alternative elected = 3
2022/11/07 18:26:26 [GET][id46] requesting result of election [vote_kemeny], Alternative elected = 4
2022/11/07 18:26:26 [GET][id95] requesting result of election [vote_stv], Alternative elected = 3
2022/11/07 18:26:26 [GET][id32] requesting result of election [vote_kemeny], Alternative elected = 4
2022/11/07 18:26:26 [GET][id84] requesting result of election [vote_stv], Alternative elected = 3
2022/11/07 18:26:26 [GET][id42] requesting result of election [vote_stv], Alternative elected = 3
2022/11/07 18:26:26 [GET][id23] requesting result of election [vote_kemeny], Alternative elected = 4
2022/11/07 18:26:26 [GET][id14] requesting result of election [vote_kemeny], Alternative elected = 4
2022/11/07 18:26:26 [GET][id98] requesting result of election [vote_kemeny], Alternative elected = 4
2022/11/07 18:26:26 [GET][id43] requesting result of election [vote_stv], Alternative elected = 3
2022/11/07 18:26:26 [GET][id36] requesting result of election [vote_stv], Alternative elected = 3
2022/11/07 18:26:26 [GET][id89] requesting result of election [vote_stv], Alternative elected = 3
2022/11/07 18:26:26 [GET][id08] requesting result of election [vote_approval], Alternative elected = 3
2022/11/07 18:26:26 [GET][id69] requesting result of election [vote_kemeny], Alternative elected = 4
2022/11/07 18:26:26 [GET][id12] requesting result of election [vote_kemeny], Alternative elected = 4
2022/11/07 18:26:26 [GET][id48] requesting result of election [vote_kemeny], Alternative elected = 4
2022/11/07 18:26:26 [GET][id71] requesting result of election [vote_kemeny], Alternative elected = 4
2022/11/07 18:26:26 [GET][id11] requesting result of election [vote_kemeny], Alternative elected = 4
2022/11/07 18:26:26 [GET][id66] requesting result of election [vote_kemeny], Alternative elected = 4
2022/11/07 18:26:26 [GET][id17] requesting result of election [vote_kemeny], Alternative elected = 4
2022/11/07 18:26:26 [GET][id94] requesting result of election [vote_kemeny], Alternative elected = 4
2022/11/07 18:26:26 [GET][id25] requesting result of election [vote_kemeny], Alternative elected = 4
2022/11/07 18:26:26 [GET][id78] requesting result of election [vote_kemeny], Alternative elected = 4
2022/11/07 18:26:26 [GET][id06] requesting result of election [vote_kemeny], Alternative elected = 4
2022/11/07 18:26:26 [GET][id28] requesting result of election [vote_kemeny], Alternative elected = 4
2022/11/07 18:26:26 [GET][id41] requesting result of election [vote_kemeny], Alternative elected = 4
2022/11/07 18:26:26 [GET][id57] requesting result of election [vote_kemeny], Alternative elected = 4
2022/11/07 18:26:26 [GET][id68] requesting result of election [vote_kemeny], Alternative elected = 4
2022/11/07 18:26:26 [GET][id83] requesting result of election [vote_kemeny], Alternative elected = 4
2022/11/07 18:26:26 [GET][id63] requesting result of election [vote_kemeny], Alternative elected = 4
2022/11/07 18:26:26 [GET][id04] requesting result of election [vote_kemeny], Alternative elected = 4
2022/11/07 18:26:26 [GET][id67] requesting result of election [vote_kemeny], Alternative elected = 4
2022/11/07 18:26:26 [GET][id86] requesting result of election [vote_kemeny], Alternative elected = 4
2022/11/07 18:26:26 [GET][id37] requesting result of election [vote_kemeny], Alternative elected = 4
2022/11/07 18:26:26 [GET][id73] requesting result of election [vote_stv], Alternative elected = 3
2022/11/07 18:26:26 [GET][id21] requesting result of election [vote_kemeny], Alternative elected = 4
2022/11/07 18:26:26 [GET][id84] requesting result of election [vote_kemeny], Alternative elected = 4
2022/11/07 18:26:26 [GET][id95] requesting result of election [vote_kemeny], Alternative elected = 4
2022/11/07 18:26:26 [GET][id42] requesting result of election [vote_kemeny], Alternative elected = 4
2022/11/07 18:26:26 [GET][id43] requesting result of election [vote_kemeny], Alternative elected = 4
2022/11/07 18:26:26 [GET][id36] requesting result of election [vote_kemeny], Alternative elected = 4
2022/11/07 18:26:26 [GET][id08] requesting result of election [vote_stv], Alternative elected = 3
2022/11/07 18:26:26 [GET][id89] requesting result of election [vote_kemeny], Alternative elected = 4
2022/11/07 18:26:26 [GET][id73] requesting result of election [vote_kemeny], Alternative elected = 4
2022/11/07 18:26:26 [GET][id08] requesting result of election [vote_kemeny], Alternative elected = 4
2022/11/07 18:26:36 [GET][id49] requesting server request count, count = 505
2022/11/07 18:26:36 [GET][id40] requesting server request count, count = 505
2022/11/07 18:26:36 [GET][id82] requesting server request count, count = 505
2022/11/07 18:26:36 [GET][id03] requesting server request count, count = 505
2022/11/07 18:26:36 [GET][id01] requesting server request count, count = 505
2022/11/07 18:26:36 [GET][id64] requesting server request count, count = 505
2022/11/07 18:26:36 [GET][id58] requesting server request count, count = 505
2022/11/07 18:26:36 [GET][id60] requesting server request count, count = 505
2022/11/07 18:26:36 [GET][id54] requesting server request count, count = 505
2022/11/07 18:26:36 [GET][id62] requesting server request count, count = 505
2022/11/07 18:26:36 [GET][id30] requesting server request count, count = 505
2022/11/07 18:26:36 [GET][id93] requesting server request count, count = 505
2022/11/07 18:26:36 [GET][id10] requesting server request count, count = 505
2022/11/07 18:26:36 [GET][id18] requesting server request count, count = 505
2022/11/07 18:26:36 [GET][id88] requesting server request count, count = 505
2022/11/07 18:26:36 [GET][id02] requesting server request count, count = 505
2022/11/07 18:26:36 [GET][id96] requesting server request count, count = 505
2022/11/07 18:26:36 [GET][id99] requesting server request count, count = 505
2022/11/07 18:26:36 [GET][id70] requesting server request count, count = 505
2022/11/07 18:26:36 [GET][id05] requesting server request count, count = 505
2022/11/07 18:26:36 [GET][id77] requesting server request count, count = 505
2022/11/07 18:26:36 [GET][id80] requesting server request count, count = 505
2022/11/07 18:26:36 [GET][id27] requesting server request count, count = 505
2022/11/07 18:26:36 [GET][id92] requesting server request count, count = 505
2022/11/07 18:26:36 [GET][id20] requesting server request count, count = 505
2022/11/07 18:26:36 [GET][id22] requesting server request count, count = 505
2022/11/07 18:26:36 [GET][id85] requesting server request count, count = 505
2022/11/07 18:26:36 [GET][id90] requesting server request count, count = 505
2022/11/07 18:26:36 [GET][id50] requesting server request count, count = 505
2022/11/07 18:26:36 [GET][id52] requesting server request count, count = 505
2022/11/07 18:26:36 [GET][id65] requesting server request count, count = 505
2022/11/07 18:26:36 [GET][id81] requesting server request count, count = 505
2022/11/07 18:26:36 [GET][id59] requesting server request count, count = 505
2022/11/07 18:26:36 [GET][id00] requesting server request count, count = 505
2022/11/07 18:26:36 [GET][id13] requesting server request count, count = 505
2022/11/07 18:26:36 [GET][id45] requesting server request count, count = 505
2022/11/07 18:26:36 [GET][id19] requesting server request count, count = 505
2022/11/07 18:26:36 [GET][id51] requesting server request count, count = 505
2022/11/07 18:26:36 [GET][id87] requesting server request count, count = 505
2022/11/07 18:26:36 [GET][id79] requesting server request count, count = 505
2022/11/07 18:26:36 [GET][id07] requesting server request count, count = 505
2022/11/07 18:26:36 [GET][id24] requesting server request count, count = 505
2022/11/07 18:26:36 [GET][id44] requesting server request count, count = 505
2022/11/07 18:26:36 [GET][id72] requesting server request count, count = 505
2022/11/07 18:26:36 [GET][id47] requesting server request count, count = 505
2022/11/07 18:26:36 [GET][id53] requesting server request count, count = 505
2022/11/07 18:26:36 [GET][id29] requesting server request count, count = 505
2022/11/07 18:26:36 [GET][id55] requesting server request count, count = 505
2022/11/07 18:26:36 [GET][id34] requesting server request count, count = 505
2022/11/07 18:26:36 [GET][id38] requesting server request count, count = 505
2022/11/07 18:26:36 [GET][id16] requesting server request count, count = 505
2022/11/07 18:26:36 [GET][id91] requesting server request count, count = 505
2022/11/07 18:26:36 [GET][id56] requesting server request count, count = 505
2022/11/07 18:26:36 [GET][id15] requesting server request count, count = 505
2022/11/07 18:26:36 [GET][id61] requesting server request count, count = 505
2022/11/07 18:26:36 [GET][id75] requesting server request count, count = 505
2022/11/07 18:26:36 [GET][id39] requesting server request count, count = 505
2022/11/07 18:26:36 [GET][id97] requesting server request count, count = 505
2022/11/07 18:26:36 [GET][id46] requesting server request count, count = 505
2022/11/07 18:26:36 [GET][id35] requesting server request count, count = 505
2022/11/07 18:26:36 [GET][id32] requesting server request count, count = 505
2022/11/07 18:26:36 [GET][id74] requesting server request count, count = 505
2022/11/07 18:26:36 [GET][id23] requesting server request count, count = 505
2022/11/07 18:26:36 [GET][id76] requesting server request count, count = 505
2022/11/07 18:26:36 [GET][id31] requesting server request count, count = 505
2022/11/07 18:26:36 [GET][id09] requesting server request count, count = 505
2022/11/07 18:26:36 [GET][id33] requesting server request count, count = 505
2022/11/07 18:26:36 [GET][id14] requesting server request count, count = 505
2022/11/07 18:26:36 [GET][id98] requesting server request count, count = 505
2022/11/07 18:26:36 [GET][id26] requesting server request count, count = 505
2022/11/07 18:26:36 [GET][id48] requesting server request count, count = 505
2022/11/07 18:26:36 [GET][id66] requesting server request count, count = 505
2022/11/07 18:26:36 [GET][id17] requesting server request count, count = 505
2022/11/07 18:26:36 [GET][id94] requesting server request count, count = 505
2022/11/07 18:26:36 [GET][id28] requesting server request count, count = 505
2022/11/07 18:26:36 [GET][id69] requesting server request count, count = 505
2022/11/07 18:26:36 [GET][id12] requesting server request count, count = 505
2022/11/07 18:26:36 [GET][id25] requesting server request count, count = 505
2022/11/07 18:26:36 [GET][id78] requesting server request count, count = 505
2022/11/07 18:26:36 [GET][id04] requesting server request count, count = 505
2022/11/07 18:26:36 [GET][id06] requesting server request count, count = 505
2022/11/07 18:26:36 [GET][id83] requesting server request count, count = 505
2022/11/07 18:26:36 [GET][id57] requesting server request count, count = 505
2022/11/07 18:26:36 [GET][id37] requesting server request count, count = 505
2022/11/07 18:26:36 [GET][id68] requesting server request count, count = 505
2022/11/07 18:26:36 [GET][id95] requesting server request count, count = 505
2022/11/07 18:26:36 [GET][id21] requesting server request count, count = 505
2022/11/07 18:26:36 [GET][id42] requesting server request count, count = 505
2022/11/07 18:26:36 [GET][id84] requesting server request count, count = 505
2022/11/07 18:26:36 [GET][id71] requesting server request count, count = 505
2022/11/07 18:26:36 [GET][id67] requesting server request count, count = 505
2022/11/07 18:26:36 [GET][id86] requesting server request count, count = 505
2022/11/07 18:26:36 [GET][id41] requesting server request count, count = 505
2022/11/07 18:26:36 [GET][id63] requesting server request count, count = 505
2022/11/07 18:26:36 [GET][id89] requesting server request count, count = 505
2022/11/07 18:26:36 [GET][id43] requesting server request count, count = 505
2022/11/07 18:26:36 [GET][id36] requesting server request count, count = 505
2022/11/07 18:26:36 [GET][id73] requesting server request count, count = 505
2022/11/07 18:26:36 [GET][id11] requesting server request count, count = 505
2022/11/07 18:26:36 [GET][id08] requesting server request count, count = 505
```